package lesqllog

import (
	"bytes"
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"text/template"
	"time"

	"github.com/muesli/termenv"
	"github.com/spf13/afero"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

const (
	colorDebug     = "129" // Violet
	colorImportant = "69"  // Cyan
	colorSuccess   = "34"  // Green
	colorError     = "196" // Red
	colorWarn      = "11"  // Yellow
)

var (
	possibleJobColors = []string{
		"70",  // light green
		"52",  // dark red
		"163", // pink
		"214", // yellow
		"22",  // green
		"208", // orange
		"55",  // violet
		"18",  // dark blue
		"94",  // ocher
		"33",  // light blue
		"197", // light red
	}
)

type logEntry struct {
	StartTs    string
	EndTs      string
	NodeName   string
	ScriptArgs string
	Status     string
	JobID      string
	Message    string
	Project    string
	Runtime    int
}

type Logger struct {
	Level          string
	LogFile        string
	LogToFile      bool
	LogEntry       string
	StartTs        time.Time
	EndTs          time.Time
	AferoFs        *afero.Afero
	JobDecorations jobDecorations
	JobCount       int
}

var jobColorsMutex sync.RWMutex

type jobDecorationStruct struct {
	Color string
	Char  string
}

type jobDecorations map[string]jobDecorationStruct

// New creates new logger instance
func New() *Logger {

	fs := afero.NewOsFs()
	lggr := &Logger{
		Level:          "info",
		LogToFile:      false,
		LogFile:        "",
		LogEntry:       "{{ .EndTs }} {{ .Status }} {{ .Message }}",
		StartTs:        time.Now(),
		AferoFs:        &afero.Afero{Fs: fs},
		JobDecorations: make(jobDecorations),
	}
	return lggr
}

// SetLevel sets logging level
func (logger *Logger) SetLevel(lvl string) error {
	level, err := setLevel(lvl)
	if err != nil {
		return err
	}
	logger.Level = level
	return nil
}

func setLevel(lvl string) (string, error) {
	switch strings.ToLower(lvl) {
	case "quiet":
		return lvl, nil
	case "panic":
		return lvl, nil
	case "fatal":
		return lvl, nil
	case "error":
		return lvl, nil
	case "warn", "warning":
		return "warn", nil
	case "info":
		return lvl, nil
	case "debug":
		return lvl, nil
	case "trace":
		return lvl, nil
	}
	return "", fmt.Errorf("not a valid log Level: %q", lvl)
}

// SetLogFile sets
func (logger *Logger) SetLogFile(logfile string) error {

	//Just test, if file exists and is writeable
	aferoFile := openFile(logger, logfile)
	aferoFile.Close()

	logger.LogToFile = true
	logger.LogFile = logfile
	return nil
}

// DeactivateLogFile deactivates logfile
func (logger *Logger) DeactivateLogFile() {
	logger.LogToFile = false
}

// SetLogEntry sets the format of each log entry. It is a go text/template
func (logger *Logger) SetLogEntry(logentry string) error {
	if logentry != "" {
		logger.LogEntry = logentry
	}
	return nil
}

func (logger *Logger) PrintJob(jobID string, args ...interface{}) {
	if logger.Level == "debug" || logger.Level == "info" || logger.Level == "warning" {
		jobIDAnsi := addJobColor(logger, jobID)
		args := append([]interface{}{jobIDAnsi}, args...) //Prepend jobIDAnsi
		fmt.Print(args...)
	}

}

func (logger *Logger) PrintlnJob(jobID string, args ...interface{}) {
	if logger.Level == "debug" || logger.Level == "info" || logger.Level == "warning" {
		jobIDAnsi := addJobColor(logger, jobID)
		args := append([]interface{}{jobIDAnsi}, args...) //Prepend jobIDAnsi
		fmt.Println(args...)
	}
}

func (logger *Logger) WarninglnJob(jobID string, args ...interface{}) {
	if logger.Level == "debug" || logger.Level == "info" || logger.Level == "warning" {
		p := termenv.ANSI256
		jobIDAnsi := addJobColor(logger, jobID)
		fmt.Printf("%s%s: %s\n", jobIDAnsi, jobID, termenv.String(fmt.Sprint(args...)).Foreground(p.Color(colorWarn)).Bold().String())
	}
}

func (logger *Logger) PrintImportantJob(jobID string, args ...interface{}) {
	if logger.Level == "debug" || logger.Level == "info" || logger.Level == "warning" {
		p := termenv.ANSI256
		jobIDAnsi := addJobColor(logger, jobID)
		fmt.Printf("%s%s: %s\n", jobIDAnsi, jobID, termenv.String(fmt.Sprint(args...)).Foreground(p.Color(colorImportant)).String())
	}
}

func (logger *Logger) FatalJob(project string, jobID string, args ...interface{}) {
	if logger.Level == "debug" || logger.Level == "info" || logger.Level == "warning" {
		jobIDAnsi := addJobColor(logger, jobID)
		args := append([]interface{}{jobIDAnsi}, args...) //Prepend jobIDAnsi
		logger.Fatal(project, args...)
	}
}

func (logger *Logger) Print(args ...interface{}) {
	if logger.Level == "debug" || logger.Level == "info" || logger.Level == "warning" {
		fmt.Print(fmt.Sprint(args...))
	}
}

func (logger *Logger) Println(args ...interface{}) {
	if logger.Level == "debug" || logger.Level == "info" || logger.Level == "warning" {
		fmt.Println(fmt.Sprint(args...))
	}
}

func (logger *Logger) Warningln(args ...interface{}) {
	if logger.Level == "debug" || logger.Level == "info" || logger.Level == "warning" {
		p := termenv.ANSI256
		fmt.Println(termenv.String(fmt.Sprint(args...)).Foreground(p.Color(colorWarn)).Bold().String())
	}
}

func (logger *Logger) PrintImportant(args ...interface{}) {
	if logger.Level == "debug" || logger.Level == "info" || logger.Level == "warning" {
		p := termenv.ANSI256
		fmt.Printf("%s\n", termenv.String(fmt.Sprint(args...)).Foreground(p.Color(colorImportant)).String())
	}
}

func (logger *Logger) Debug(args ...interface{}) {
	if logger.Level == "debug" {
		p := termenv.ANSI256
		fmt.Printf("%s\n", termenv.String(fmt.Sprint(args...)).Foreground(p.Color(colorDebug)).String())
	}
}

func (logger *Logger) SqlDebug(jobID string, sql string, name string) {
	if logger.Level == "debug" {

		p := termenv.ANSI256

		jobIDAnsi := addJobColor(logger, jobID) + jobID

		sqlDebug := strings.Split(sql, "\n")

		fmt.Println(jobIDAnsi + ": " + termenv.String("-- SQL START                        ").Foreground(p.Color("7")).Background(p.Color("240")).String())

		for lineID, lineString := range sqlDebug {

			lineIDString := termenv.String(fmt.Sprintf("%4d", lineID+1)).Foreground(p.Color("7")).Background(p.Color("240")).String()

			fmt.Println(jobIDAnsi + ":" + " " + lineIDString + " " + lineString)
		}

		fmt.Println(jobIDAnsi + ": " + termenv.String("-- SQL END                          ").Foreground(p.Color("7")).Background(p.Color("240")).String())

		fmt.Println(jobIDAnsi)
	}
}

// Success prints success message and exists with exitcode 0
func (logger *Logger) Success(project string, message interface{}) {

	logger.EndTs = time.Now()

	if logger.Level == "debug" || logger.Level == "info" || logger.Level == "warning" {
		p := termenv.ANSI256
		fmt.Println(termenv.String(fmt.Sprint(message)).Foreground(p.Color(colorSuccess)).Bold().String())
		fmt.Println()
	}
	if logger.LogToFile {

		duration := logger.EndTs.Sub(logger.StartTs)

		logData := logEntry{
			StartTs: logger.StartTs.Format(time.RFC3339),
			EndTs:   logger.EndTs.Format(time.RFC3339),
			Status:  "success",
			Project: project,
			Runtime: int(duration.Nanoseconds() / 1000 / 1000),
		}
		writeLogFile(logger, logData)

	}
	os.Exit(0)
}

// Fatal prints error message and exists with exitcode 1
func (logger *Logger) Fatal(project string, args ...interface{}) {
	p := termenv.ANSI256
	if len(args) > 1 {
		message := ""
		for argI, arg := range args {
			if argI != 0 {
				str := fmt.Sprintf("%s", arg)
				message += termenv.String(str).Foreground(p.Color(colorError)).Bold().String()
			}
		}
		fmt.Printf("%s%s\n", args[0], message)
	} else {
		fmt.Printf("%s\n", termenv.String(fmt.Sprint(args...)).Foreground(p.Color(colorError)).Bold().String())
	}
	println(fmt.Sprint(args...))
	println() //println without fmt prints to standard error

	if logger.LogToFile {
		logger.EndTs = time.Now()

		duration := logger.EndTs.Sub(logger.StartTs)

		message := strings.Replace(fmt.Sprint(args...), `"`, `\"`, -1)

		logData := logEntry{
			StartTs: logger.StartTs.Format(time.RFC3339),
			EndTs:   logger.EndTs.Format(time.RFC3339),
			Status:  "error",
			Project: project,
			Runtime: int(duration.Nanoseconds() / 1000 / 1000),
			Message: message,
		}
		writeLogFile(logger, logData)
	}

	os.Exit(1)
}

func openFile(logger *Logger, logfile string) afero.File {
	var aferoFile afero.File

	if ok, _ := logger.AferoFs.Exists(logfile); !ok {
		logger.Debug("Logfile does not exists. Try to create new one: ", logfile)
		logpath := filepath.Dir(logfile)
		ok, err := logger.AferoFs.Exists(logpath)
		if err != nil {
			logger.DeactivateLogFile()
			logger.Fatal("Can not open logpath: ", err)
		}
		if !ok {
			logger.DeactivateLogFile()
			logger.Fatal("Logpath does not exists ", logpath)
		}
		logger.Debug("Creating logfile: ", logfile)
		_, err = logger.AferoFs.Create(logfile)
		if err != nil {
			logger.DeactivateLogFile()
			logger.Fatal("Cannot create logfile: ", err)
		}
		aferoFile, err = logger.AferoFs.OpenFile(logfile, os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			logger.DeactivateLogFile()
			logger.Fatal("Cannot open logfile: ", err)
		}
	} else {
		var err error
		logger.Debug("Opening logfile: ", logfile)
		aferoFile, err = logger.AferoFs.OpenFile(logfile, os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			logger.DeactivateLogFile()
			logger.Fatal("Cannot open logfile: ", err)
		}
	}

	return aferoFile
}

func writeLogFile(logger *Logger, logData logEntry) {
	var (
		tpl bytes.Buffer
	)
	aferoFile := openFile(logger, logger.LogFile)
	t := template.New("logEntry")
	t, err := t.Parse(logger.LogEntry)
	if err != nil {
		logger.DeactivateLogFile()
		logger.Fatal(fmt.Sprintf("LogEntry template parse: %s", err))
	}

	if err := t.Execute(&tpl, logData); err != nil {
		logger.DeactivateLogFile()
		logger.Fatal(fmt.Sprintf("LogEntry template execute: %s", err))
	}

	if _, err := aferoFile.WriteString(tpl.String() + "\n"); err != nil {
		logger.DeactivateLogFile()
		logger.Fatal(fmt.Sprintf("LogEntry write string to file: %s", err))
	}
}

func addJobColor(logger *Logger, jobID string) string {
	p := termenv.ANSI256

	jobColorsMutex.RLock()
	jobDecoration, ok := logger.JobDecorations[jobID]
	jobColorsMutex.RUnlock()
	if !ok {
		jobColorsMutex.Lock()

		jobDecoration = jobDecorationStruct{
			Color: possibleJobColors[rand.Intn(len(possibleJobColors))],
			Char:  RandStringRunes(2),
		}

		logger.JobDecorations[jobID] = jobDecoration
		logger.JobCount++
		jobColorsMutex.Unlock()

	}

	return (termenv.String(jobDecoration.Char).Foreground(p.Color("7")).Background(p.Color(jobDecoration.Color)).String() + " ")
}

var letterRunes = []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
