package project

import (
	"fmt"
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
	"gitlab.com/lesql/lesql/src/lesqllog"
	"gitlab.com/lesql/lesql/src/libs/workflow"
	"gopkg.in/yaml.v2"
)

var vipercfg *viper.Viper
var logger *lesqllog.Logger

type jobYaml struct {
	Name    string `yaml:"name"`
	Tags    []jobTagYaml
	Type    string
	Parents []string `yaml:"parents,omitempty"`
	Exec    string
}

type jobTagYaml struct {
	Text  string
	Color string
}

// ShowWorkflow prints the workflow as yaml file to STDOUT
func ShowWorkflow(projectID string, vipercfg2 *viper.Viper, lggr *lesqllog.Logger) error {

	//Make vipercfg global in this package
	vipercfg = vipercfg2

	logger = lggr
	wf, err := workflow.GetWorkflow(vipercfg.GetString("basepath"), vipercfg.GetString("modelspath"), vipercfg.GetString("modelsglobalpath"), vipercfg.GetString("macrospath"), projectID, vipercfg.GetString("modelspathsuffix"), logger, "postgresql")

	if err != nil {
		logger.Fatal(projectID, err)
		return err
	}

	jobsYaml := []jobYaml{}
	logger.Debug("")
	logger.Debug("=============================")
	for jobName, job := range wf.Jobs {
		logger.Debug("")
		logger.Debug("Job: ", jobName)

		parents := make([]string, 0)

		//Do not add jobs with no parents into export => these jobs have to be defined in queue
		if len(job.ParentJobs) == 0 && job.Data["doNotIgnoreWithoutRefs"] == "" {
			logger.Debug("WARN: ", jobName, ": Has no parentJobs and 'doNotIgnoreWithoutRefs' not set => ignoring")
			continue
		} else {
			//logger.Debug("INFO: ", jobName, ": Has ", len(job.ParentJobs), " parent jobs")
		}

		for _, parent := range job.ParentJobs {
			parents = append(parents, parent.Name)
		}

		var tags []jobTagYaml

		tagsSlice := []string{}

		if job.Data["tags"] != "" {
			tagsSlice = strings.Split(job.Data["tags"], "__|X|__")
		}

		tagColorsSlice := strings.Split(job.Data["tagColors"], "__|X|__")

		for id, tag := range tagsSlice {

			var color string
			if len(tagColorsSlice) > id {
				color = tagColorsSlice[id]
			}
			tags = append(tags, jobTagYaml{Text: tag, Color: color})
		}

		jobYaml := jobYaml{
			Name:    jobName,
			Parents: parents,
			Tags:    tags,
			Type:    "exec",
			Exec:    "lesql run " + projectID + " -m " + filepath.ToSlash(job.Data["modelPath"]),
		}

		//fmt.Printf("%+v\n", jobYaml)

		jobsYaml = append(jobsYaml, jobYaml)
	}

	export := struct {
		Workflow []jobYaml
	}{
		Workflow: jobsYaml,
	}

	exportBytes, err := yaml.Marshal(export)

	fmt.Println(string(exportBytes))

	return nil
}
