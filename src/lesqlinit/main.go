package lesqlinit

import (
	"fmt"
	"path/filepath"

	"github.com/spf13/afero"
	"github.com/spf13/viper"
	"gitlab.com/lesql/lesql/src/lesqllog"
	"gitlab.com/lesql/lesql/src/libs/cryptrandom"
)

const configFile = `modelspath = "workflows"
modelspathsuffix = "models"
macrospath = "globals/macros"
modelsglobalpath = "globals/models"
logfile = "lesql.jsonl"
logentry = '{"timestamp_start":"{{ .StartTs }}","timestamp_log":"{{ .EndTs }}","node_name":"{{ .NodeName }}","script_name":"lesql","script_args":"{{ .ScriptArgs }}","type":"{{ .Status }}", "project" : "{{ .Project }}","job_id":"{{ .JobID }}","runtime": {{ .Runtime }},"message":"{{ .Message }}","trace":""}'

[storageprovider]
driver = "yaml"

[yaml]
location = "./workflows/{{ .ProjectName }}/dbauth.yml"
`

const exampleModelFile = `{{ block "CONFIG" .}}
{{end}}

{{ block "SELECT" .}}
SELECT * FROM {{ .Project }}.{{ ref "test1" }}
WHERE "date" BETWEEN '{{ .StartDate }}' and '{{ .EndDate }}'
{{end}}
`

const exampleDbAuth = `{
  "type": "pgsql",
  "host": "localhost",
  "port": "5432",
  "user": "db_zzzexampleproject",
  "pass": "%s",
  "db": "zzzexampleproject"
}`

const setupExampleDatabase = `
DROP DATABASE IF EXISTS zzzexampleproject;
CREATE DATABASE zzzexampleproject;
\c zzzexampleproject
CREATE SCHEMA "zzzexampleproject";

DROP ROLE IF EXISTS "db_zzzexampleproject";
CREATE ROLE "db_zzzexampleproject" LOGIN;

GRANT CONNECT ON DATABASE "zzzexampleproject" TO "db_zzzexampleproject";
GRANT USAGE ON SCHEMA "zzzexampleproject" TO "db_zzzexampleproject";
GRANT CREATE ON SCHEMA "zzzexampleproject" TO "db_zzzexampleproject";
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA "zzzexampleproject" TO "db_zzzexampleproject";
GRANT USAGE ON ALL SEQUENCES IN SCHEMA "zzzexampleproject" TO "db_zzzexampleproject";

ALTER DEFAULT PRIVILEGES IN SCHEMA "zzzexampleproject" GRANT ALL PRIVILEGES ON TABLES TO "db_zzzexampleproject";
ALTER DEFAULT PRIVILEGES IN SCHEMA "zzzexampleproject" GRANT USAGE ON SEQUENCES TO "db_zzzexampleproject";

ALTER USER "db_zzzexampleproject" WITH ENCRYPTED PASSWORD '%s';

SET ROLE "db_zzzexampleproject";

CREATE TABLE "zzzexampleproject"."test1" (
	dimension1 varchar NOT NULL,
	dimension2 varchar NOT NULL,
	kpi1 numeric NOT NULL,
	kpi2 numeric NOT NULL,
	"date" date NOT NULL
);

INSERT INTO "zzzexampleproject"."test1" VALUES
('a1', 'a2', 105, 3469, current_date),
('a1', 'b2', 34, 7456, current_date),
('b1', 'a1', 76, 2446, current_date),
('b1', 'b2', 76, 7222, current_date)
;
`

const helpTextsuccess = `Your init was successful

Created a new password for encrypted values in dbauth.yml files: %s
!!!! Save this password in password manager like keepass or gopass !!!!

Now head over to https://lesql.com/docs/getting-started/#3 to

OR

If you are on Ubuntu 18.04, just follow the steps you run your first model:

----

# Install Postgresql 10 if not installed already
sudo apt install postgresql-10

# Add a project called 'zzzexampleproject' to your database
sudo -u postgres psql < quickstart-zzzexampleproject.sql
exit

# Now you should delete the quickstart-zzzexampleproject.sql as it is not needed anymore
rm quickstart-zzzexampleproject.sql

# Set an environment variable with your password
echo "%s" > ~/.lesql_yaml_password
export LESQL_YAML_PASSWORD="$(cat ~/.lesql_yaml_password)"

# Now you can run your first model
go run main.go run zzzexampleproject -d

----

Head over to https://lesql.com/docs/getting-started/#4 to learn more on lesql
`

var vipercfg *viper.Viper
var logger *lesqllog.Logger

//Run runs the main init function
func Run(vipercfg2 *viper.Viper, lggr *lesqllog.Logger) {
	logger = lggr
	vipercfg = vipercfg2
	var fs = afero.NewOsFs()

	logger.Print(fmt.Sprintf("Test if config file exists: %s\n", vipercfg.ConfigFileUsed()))

	afs := &afero.Afero{Fs: fs}
	exists, err := afs.Exists(vipercfg.ConfigFileUsed())
	if err != nil {
		logger.Fatal("", err, "\n")
	}
	if exists {
		logger.Fatal("", "\nConfig file already exists: \n    "+vipercfg.ConfigFileUsed()+"\n\nWill not init project!\n")
	}

	dbAuthPassword, err := cryptrandom.CreateRandom(18)
	if err != nil {
		logger.Fatal("", "Cannot create password for config-lesql.toml.\n")
	}

	exampleDbAuthPassword, err := cryptrandom.CreateRandom(18)
	if err != nil {
		logger.Fatal("", "Cannot create password for config-lesql.toml.\n")
	}
	exampleDbAuthHash, err := cryptrandom.EncryptString(vipercfg.GetString("cryptsalt"), dbAuthPassword, exampleDbAuthPassword)
	if err != nil {
		logger.Fatal("", "Cannot encrypt db password for zzzexampleproject.\n")
	}

	baseDir, _ := filepath.Abs(filepath.Dir(vipercfg.ConfigFileUsed()))

	logger.Print(fmt.Sprintf("Init lesql in: %s\n", baseDir))
	if err != nil {
		logger.Fatal("", err, "\n")
	}

	globals := filepath.Join(baseDir, "globals")
	afs.Mkdir(globals, 0755)

	modelsGlobal := filepath.Join(globals, "models")
	afs.Mkdir(modelsGlobal, 0755)

	macros := filepath.Join(globals, "macros")
	afs.Mkdir(macros, 0755)

	workflows := filepath.Join(baseDir, "workflows")
	afs.Mkdir(workflows, 0755)

	exampleProject := filepath.Join(workflows, "zzzexampleproject")
	afs.Mkdir(exampleProject, 0755)

	exampleProjectModels := filepath.Join(exampleProject, "models")
	afs.Mkdir(exampleProjectModels, 0755)

	afs.WriteFile(filepath.Join(baseDir, "config-lesql.toml"), []byte(configFile), 0644)
	afs.WriteFile(filepath.Join(exampleProjectModels, "001-test2.le.sql"), []byte(exampleModelFile), 0644)

	afs.WriteFile(filepath.Join(exampleProject, "dbauth.yml"), []byte(fmt.Sprintf(exampleDbAuth, exampleDbAuthHash)), 0644)
	afs.WriteFile(filepath.Join(baseDir, "quickstart-zzzexampleproject.sql"), []byte(fmt.Sprintf(setupExampleDatabase, exampleDbAuthPassword)), 0644)

	fmt.Printf(helpTextsuccess, dbAuthPassword, dbAuthPassword)

}
