package get

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"github.com/spf13/viper"
	"gitlab.com/lesql/lesql/src/lesqllog"
)

//Projects returns a list of all projects used for auto complete
func Projects(vipercfg2 *viper.Viper, lggr *lesqllog.Logger) error {
	vipercfg = vipercfg2
	modelPath := path.Join(vipercfg.GetString("basepath"), vipercfg.GetString("modelspath"))

	fi, err := os.Stat(modelPath)
	if err != nil {
		logger.Fatal("Modelpath does not exist: " + modelPath)
		return fmt.Errorf("Modelpath does not exist: " + modelPath)
	}

	if fi.Mode().IsDir() {

		files, err := ioutil.ReadDir(modelPath)
		if err != nil {
			return err
		}

		for _, file := range files {
			fmt.Printf("%s ", file.Name())
		}
		fmt.Println()

	} else {
		return fmt.Errorf("Modelpath is not a directory: " + modelPath)
	}

	return nil
}
