package run

import (
	"database/sql"
	"errors"
	"math/rand"
	"os"
	"path"
	"time"

	"gitlab.com/lesql/dag"
	"gitlab.com/lesql/lesql/src/lesqllog"
	"gitlab.com/lesql/lesql/src/libs/cfgprovider"
	"gitlab.com/lesql/lesql/src/libs/config"
	"gitlab.com/lesql/lesql/src/libs/dbprovider"
	"gitlab.com/lesql/lesql/src/libs/workflow"

	"github.com/spf13/viper"
)

var vipercfg *viper.Viper
var logger *lesqllog.Logger

type Workflow struct {
	ProjectName string          `json:"projectName"`
	Nodes       []dag.JobBasics `json:"nodes"`
	Edges       []dag.Edge      `json:"edges"`
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

// Run is the main entry point to run models
func Run(projectName string, vipercfg2 *viper.Viper, lggr *lesqllog.Logger) {

	//Make vipercfg global in this package
	vipercfg = vipercfg2
	logger = lggr

	var databaseCredentials cfgprovider.DatabaseCredentials

	//govalidator.IsURL(`http://user@pass:domain.com/path/page`)

	model := vipercfg.GetString("model")

	modelspath, err := config.GetConfigPath(vipercfg.GetString("modelspath"), projectName, vipercfg.GetString("modelspathsuffix"))
	if err != nil {
		logger.Fatal(projectName, "Can not create modelsPath: "+vipercfg.GetString("modelspath"))
		return
	}

	logger.Debug("Model-Param: '" + model + "'")

	modelToRun := path.Join(vipercfg.GetString("basepath"), modelspath, model)

	fi, err := os.Stat(modelToRun)
	if err != nil {
		logger.Fatal(projectName, "Transformation does not exist: "+modelToRun)
		return
	}

	logger.Println("Run project '" + projectName + "'")

	if vipercfg.GetString("storageprovider.driver") == "yaml" {
		databaseCredentials, err = getDbCredentialsFromYaml(projectName, logger)
	} else {
		if !cfgprovider.StorageProviderExists(vipercfg.GetString("storageprovider.driver")) {
			logger.Fatal("", "unknown storageprovider: "+vipercfg.GetString("storageprovider.driver"))
		}

		if vipercfg.GetString("storageprovider.password") == "" {
			logger.Fatal("", "no strorageprovider password set")
		}
		if vipercfg.GetString("storageprovider.location") == "" {
			logger.Fatal("", "no strorageprovider location set")
		}

		settings := cfgprovider.StorageProviderSettings{
			Endpoints: vipercfg.GetStringSlice("storageprovider.endpoints"),
			User:      vipercfg.GetString("storageprovider.username"),
			Password:  vipercfg.GetString("storageprovider.password"),
			Location:  vipercfg.GetString("storageprovider.location"),
			Key:       vipercfg.GetString("cryptkey"),
			KeySalt:   vipercfg.GetString("cryptsalt"),
		}
		databaseCredentials, err = cfgprovider.GetDbCredentials(vipercfg.GetString("storageprovider.driver"), projectName, settings, logger)
	}
	if err != nil {
		logger.Fatal(projectName, err)
	}

	logger.Println("Connect to database")

	logger.Println(databaseCredentials.DbType)

	if !dbprovider.DbProviderExists(databaseCredentials.DbType) {
		logger.Fatal("", "unknown dbprovider: "+databaseCredentials.DbType)
	}

	dbConnection, err := dbprovider.GetDbConnection(databaseCredentials.DbType, projectName, databaseCredentials, logger)

	if err != nil {
		logger.Fatal(projectName, err)
	}

	// if "model" is a directory, cycle through directory if it is models base path. Otherwise exit with error message
	if fi.Mode().IsDir() {

		if model != "" {
			logger.Fatal(projectName, errors.New("sorry, --model must be a file. Path given. THIS CHANGED WITH v0.20. If you have to run parts of a workflow, use 'tags'"))
		}

		logger.Debug(modelToRun + " is path")
		logger.Println("Transformations: " + path.Join(modelToRun, "*"))
		logger.Println("")

		getWorkflowAndRun(projectName, dbConnection, databaseCredentials.DbType)

		// err := filepath.Walk(modelToRun, walkThroughModelsAndTransfrom(projectName, dbConnection))
		// if err != nil {
		// 	logger.Fatal(projectName, err)
		// }

	} else {
		logger.Debug(modelToRun + " is file")
		logger.Println("Transformation: " + modelToRun)
		logger.Println("")

		transformation(modelToRun, projectName, dbConnection, databaseCredentials.DbType)
	}

	logger.Success(projectName, "Success")
}

func getWorkflowAndRun(projectName string, dbConnection *sql.DB, dbType string) {
	dag, err := workflow.CreateWorkflow(func(path string) error {

		//logger.Debug("BLUB " + projectName + " " + path)
		transformation(path, projectName, dbConnection, dbType)
		return nil
	}, vipercfg.GetString("basepath"), vipercfg.GetString("modelspath"), vipercfg.GetString("modelsglobalpath"), vipercfg.GetString("macrospath"), projectName, vipercfg.GetString("modelspathsuffix"), dbType)

	if err != nil {
		logger.Fatal(projectName, err)
	}

	err = dag.Run()

	if err != nil {
		logger.Fatal(projectName, err)
	}

}
