package run

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
	"text/template"

	"github.com/gtank/cryptopasta"
	"gitlab.com/lesql/lesql/src/lesqllog"
	"gitlab.com/lesql/lesql/src/libs/cfgprovider"
	yaml "gopkg.in/yaml.v3"
)

func getDbCredentialsFromYaml(projectName string, logger *lesqllog.Logger) (cfgprovider.DatabaseCredentials, error) {
	var yamlLocationRendered bytes.Buffer
	var databaseCredentials cfgprovider.DatabaseCredentials

	yamlLocation := vipercfg.GetString("yaml.location")

	tmpl, err := template.New("location").Parse(yamlLocation)
	if err != nil {
		logger.Fatal("", "Cannot parse yaml.location: "+yamlLocation)
	}

	yamlLocationData := struct {
		ProjectName string
	}{
		ProjectName: projectName,
	}
	err = tmpl.Execute(&yamlLocationRendered, yamlLocationData)
	if err != nil {
		logger.Fatal("", "Cannot renderyaml.location: "+yamlLocation+" "+fmt.Sprintf("%s", err))
	}

	logger.Debug("Getting db credentials from: ", yamlLocationRendered.String())

	yamlFile, err := ioutil.ReadFile(yamlLocationRendered.String())
	if err != nil {
		logger.Fatal("yamlFile.Get err   #%v", err)
	}

	err = yaml.Unmarshal(yamlFile, &databaseCredentials)
	if err != nil {
		logger.Fatal("", fmt.Sprintf("Unmarshal: %v", err))
	}

	if vipercfg.GetString("yaml.password") == "" {
		logger.Fatal("", "No 'password' set in 'yaml' section in config-lesql.toml")
	}

	env := "LESQL_YAML_PASSWORD"
	yamlKey := os.Getenv(env)
	if len(yamlKey) == 0 {
		logger.Fatal("", "Mandatory env variable not found. Use 'LESQL_YAML_PASSWORD=xxx lesql run ...'")
	}

	logger.Debug(projectName, "DbType: "+databaseCredentials.DbType)

	if databaseCredentials.DbType == "postgresql" {
		password, err := decodeDbPassword(yamlKey, databaseCredentials.Password)
		if err != nil {
			logger.Fatal(projectName, err)
		}
		databaseCredentials.Password = password
	} else if databaseCredentials.DbType == "bigquery" {
		cred, err := decodeBigqueryCredentials(yamlKey, databaseCredentials.BigQueryCredentials)
		if err != nil {
			logger.Fatal(projectName, err)
		}
		databaseCredentials.BigQueryCredentials = cred
	} else {
		logger.Fatal(projectName, "Unknown dbType: ", databaseCredentials.DbType)
	}

	return databaseCredentials, nil
}

func decodeDbPassword(yamlKey string, encodedPassword string) (string, error) {

	key := cryptopasta.Hash(vipercfg.GetString("cryptsalt"), []byte(yamlKey))
	var key32 [32]byte
	copy(key32[:], key)

	decoded, err := base64.StdEncoding.DecodeString(encodedPassword)
	if err != nil {
		return "", fmt.Errorf("Decode error of database Password: %s", err)
	}
	decodedPasswordByte, err := cryptopasta.Decrypt(decoded, &key32)
	if err != nil {
		return "", fmt.Errorf("Decrypt error of database Password: %s", err)
	}
	decodedPassword := fmt.Sprintf("%s", decodedPasswordByte)

	return decodedPassword, nil
}

func decodeBigqueryCredentials(yamlKey string, encodedCredentials string) (string, error) {

	key := cryptopasta.Hash(vipercfg.GetString("cryptsalt"), []byte(yamlKey))
	var key32 [32]byte
	copy(key32[:], key)

	decoded, err := base64.StdEncoding.DecodeString(encodedCredentials)
	if err != nil {
		return "", fmt.Errorf("Decode error of bigquery credentials: %s", err)
	}
	decodedCredentialsByte, err := cryptopasta.Decrypt(decoded, &key32)
	if err != nil {
		return "", fmt.Errorf("Decrypt error of bigquery credentials: %s", err)
	}
	decodedCredentials := fmt.Sprintf("%s", decodedCredentialsByte)

	return decodedCredentials, nil

}
