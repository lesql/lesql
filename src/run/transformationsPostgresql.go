package run

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"

	"github.com/spf13/afero"
	"gitlab.com/lesql/lesql/src/libs/templates"
)

func transformationTestSelectAndCreateSupportingSQLStatementsPostgresql(jobID string, fs afero.Fs, file string, project string, tableName string, startDate string, endDate string, templateData templates.TemplateDataStruct, dbConnection *sql.DB) (sqlStatements, error) {

	var sqlStatements sqlStatements
	var columnDefs []string
	var err error

	sqlQueries := make(map[string]string)

	sqlQueries["select"], _, err = templates.Run(fs, "select", file, templateData)
	if err != nil {
		logger.SetLevel("debug")
		logger.SqlDebug(jobID, sqlQueries["select"], tableName)
		logger.FatalJob(project, jobID, jobID+": Cannot run select: "+fmt.Sprintf("%s", err))
	}
	sqlQueries["preSelectHook"], _, err = templates.Run(fs, "preSelectHook", file, templateData)
	if err != nil {
		logger.SetLevel("debug")
		logger.SqlDebug(jobID, sqlQueries["preSelectHook"], tableName)
		logger.FatalJob(project, jobID, jobID+": Cannot run select: "+fmt.Sprintf("%s", err))
	}
	sqlQueries["postSelectHook"], _, err = templates.Run(fs, "postSelectHook", file, templateData)
	if err != nil {
		logger.SetLevel("debug")
		logger.SqlDebug(jobID, sqlQueries["postSelectHook"], tableName)
		logger.FatalJob(project, jobID, jobID+": Cannot run select: "+fmt.Sprintf("%s", err))
	}

	rows, err := dbConnection.Query(sqlQueries["select"])
	if err != nil {
		// handle this error better than this
		logger.SetLevel("debug")
		logger.SqlDebug(jobID, sqlQueries["select"], tableName)
		logger.FatalJob(project, jobID, jobID+": Can not query database: "+fmt.Sprintf("%s", err))
	}
	defer rows.Close()

	cols, err := rows.Columns()

	for _, primaryKeyColumn := range templateData.Config.PrimaryColumns {
		if !sliceContainsItem(cols, primaryKeyColumn) {
			logger.FatalJob(project, jobID, errors.New(jobID+": "+`primary key setup: column "`+primaryKeyColumn+`" does not exists`))
		}
	}

	sqlStatements.Drop = `DROP TABLE IF EXISTS "` + project + `"."` + tableName + `";`

	sqlStatements.Create = `CREATE TABLE IF NOT EXISTS "` + project + `"."` + tableName + `" ( ` + "\n"

	columnTypes, err := rows.ColumnTypes()

	hasTimestampColumn := false
	timestampColumn := templateData.Config.TimestampColumn

	for columnID, columnType := range columnTypes {

		if cols[columnID] == "?column?" {
			logger.FatalJob(project, jobID, jobID+": There are unknown column names in your SELECT query. Please be sure to name columns with 'AS' if you use static content or function like 'CONCAT'!")
		}

		if templateData.Config.Delete == true {
			if cols[columnID] == timestampColumn {
				hasTimestampColumn = true
				sqlStatements.Delete = `DELETE FROM "` + project + `"."` + tableName + `" WHERE "` + timestampColumn + `" BETWEEN '` + startDate + `' AND '` + endDate + `';`
			}
		}

		columnType := columnType.DatabaseTypeName()
		// if columnType == "UNKNOWN" {
		// 	columnType = "VARCHAR"
		// }

		columnDefs = append(columnDefs, fmt.Sprintf("  \"%s\" %s", cols[columnID], columnType))

	}
	sqlStatements.Insert = `INSERT INTO "` + project + `"."` + tableName + `" ("` + strings.Join(cols, `", "`) + `")`

	sqlStatements.Create += strings.Join(columnDefs, ",\n")
	if len(templateData.Config.PrimaryColumns) > 0 {
		sqlStatements.Create += ",\n" + ` CONSTRAINT "` + project + `_` + tableName + `_pkey" PRIMARY KEY ("` + strings.Join(templateData.Config.PrimaryColumns, `", "`) + `")` + "\n"
	}
	sqlStatements.Create += "\n);"

	if templateData.Config.Delete == true && hasTimestampColumn == false {
		logger.WarninglnJob(jobID, jobID+": No timestampColumn '"+timestampColumn+"' available => no delete running before insert")
	}

	sqlStatements.PreSelectHook = strings.Trim(sqlQueries["preSelectHook"], " ")
	//Only add ; if there is a sql statement
	if sqlStatements.PreSelectHook != "/* No pre_select_hooks */" {
		last := sqlStatements.PreSelectHook[len(sqlStatements.PreSelectHook)-1:]
		if last != ";" {
			sqlStatements.PreSelectHook += ";"
		}
	}

	sqlStatements.PostSelectHook = strings.Trim(sqlQueries["postSelectHook"], " ")
	//Only add ; if there is a sql statement
	if sqlStatements.PostSelectHook != "/* No post_select_hooks */" {
		last := sqlStatements.PostSelectHook[len(sqlStatements.PostSelectHook)-1:]
		if last != ";" {
			sqlStatements.PostSelectHook += ";"
		}
	}

	return sqlStatements, nil
}
