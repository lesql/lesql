package run

import (
	"testing"
)

func TestSliceContainsItem(t *testing.T) {

	slice := []string{"hello", "blub"}

	var tests = []struct {
		str      string // input
		expected bool   // expected result
	}{
		{"hello", true},
		{"blub", true},
		{"hello2", false},
	}

	for _, tt := range tests {
		actual := sliceContainsItem(slice, tt.str)
		if actual != tt.expected {
			t.Errorf("String '%s' not in slice %s", tt.str, slice)
		}
	}

}
