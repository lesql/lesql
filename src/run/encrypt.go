package run

import (
	"fmt"

	"github.com/spf13/viper"
	"gitlab.com/lesql/lesql/src/lesqllog"
	"gitlab.com/lesql/lesql/src/libs/cryptrandom"
)

//Encrypt is the main entry for encrypt from cmd
func Encrypt(vipercfg2 *viper.Viper, lggr *lesqllog.Logger, content string) {

	//Make vipercfg global in this package
	vipercfg = vipercfg2
	logger = lggr

	//content := ""
	yamlKey := vipercfg.GetString("yaml.password")

	if yamlKey == "" {
		logger.Fatal("yml.password not set in config")
	}

	encoded, err := cryptrandom.EncryptString(vipercfg.GetString("cryptsalt"), yamlKey, content)
	if err != nil {
		logger.Fatal("Cannot encode string")
	}

	fmt.Printf("%s\n", encoded)
}
