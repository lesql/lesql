package run

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"math/rand"
	"path/filepath"
	"strings"

	"github.com/spf13/afero"

	"gitlab.com/lesql/lesql/src/libs/templates"
)

type sqlStatements struct {
	Insert         string
	Drop           string
	Create         string
	Delete         string
	PreSelectHook  string
	PostSelectHook string
}

func transformation(file string, project string, dbConnection *sql.DB, dbType string) {

	jobID := "JOB_" + randStringBytesRmndr(6)

	var tableName string

	logger.PrintImportantJob(jobID, "Starting transformation: "+file)
	logger.PrintImportantJob(jobID, vipercfg.GetString("startDate")+" - "+vipercfg.GetString("endDate"))

	fileName := filepath.Base(file)
	fileBaseName := strings.TrimSuffix(fileName, ".le.sql")

	if strings.Contains(fileBaseName, ".") {
		logger.Fatal(project, fmt.Errorf(jobID+": No '.' is allowed in filename except in suffix '.le.sql': %s", fileName))
	}

	if strings.Contains(fileBaseName, "-") {

		/*
			aORb := regexp.MustCompile("-")
			matches := aORb.FindAllStringIndex(fileBaseName, -1)
			if len(matches) != 1 {
				logger.Fatal(project, fmt.Errorf("Only one '-' is allowed in filename: %s", fileName))
			}
		*/

		s := strings.SplitN(fileBaseName, "-", 2)
		tableName = s[1]
	} else {
		tableName = fileBaseName
	}

	templateData := templates.TemplateDataStruct{
		File:             file,
		DbType:           dbType,
		DropTargetTable:  vipercfg.GetBool("dropTargetTable"),
		Project:          project,
		TargetTable:      tableName,
		TargetSchema:     project,
		This:             "\"" + project + "\".\"" + tableName + "\"",
		StartDate:        vipercfg.GetString("startDate"),
		EndDate:          vipercfg.GetString("endDate"),
		GlobalmodelsPath: vipercfg.GetString("modelsglobalpath"),
		MacrosPath:       vipercfg.GetString("macrospath"),
		CliVars:          map[string]string{},
		Vars:             map[string]string{},
		Arrays:           map[string][]string{},
		MultiDimArrays2:  map[string]map[string][]string{},
	}

	vars := vipercfg.GetString("vars")

	if vars != "" {
		templateData.CliVars = processCliVars(project, vars)
	}

	//We run the the template 3 times:
	// 1) To read the configurations from the config block
	// 2) To generate the SELECT statement and run it against the the database => we get the
	//    columns and column types to be able to create the CREATE statement
	// 3) To generate all SQL statements (DROP, CREATE, DELETE, INSERT-SELECT) and run the template

	var fs = afero.NewOsFs()

	// --------------------------------------
	// First run to get config
	// --------------------------------------
	_, templateDataConfig, err := templates.Run(fs, "config", file, templateData)
	if err != nil {
		logger.Fatal(project, jobID+": Cannot create config: "+fmt.Sprintf("%s", err))
	}

	templateData = templates.CreateTemplateDataStruct(templateData, templateDataConfig)

	if len(templateData.Tags) > 0 {
		logger.PrintImportantJob(jobID, "Tags in model: ", "\""+strings.Join(templateData.Tags, "\", \"")+"\"")
	} else {
		logger.PrintImportantJob(jobID, "Tags in model: ", "[no tags]")
	}

	runModel := true

	tagsToRun := vipercfg.GetStringSlice("tags")
	if len(tagsToRun) > 0 {
		logger.PrintImportantJob(jobID, "Run models with following tags only: ", "\""+strings.Join(tagsToRun, "\", \"")+"\"")
		if !findTag(tagsToRun, templateData.Tags) {
			runModel = false
		}
	}

	tagsNotToRun := vipercfg.GetStringSlice("skip-tags")
	if len(tagsNotToRun) > 0 {
		logger.PrintImportantJob(jobID, "Run allmodels but not with following tags: ", "\""+strings.Join(tagsNotToRun, "\", \"")+"\"")
		if findTag(tagsNotToRun, templateData.Tags) {
			runModel = false
		}
	}

	if !runModel {
		logger.WarninglnJob(jobID, "Not running")
		logger.PrintImportantJob(jobID, "Finished")
		return
	}

	// --------------------------------------
	// Second run to get the select + pre_select_hook + post_select_hook specific for every database
	// --------------------------------------

	var sqlStatements sqlStatements
	if dbType == "bigquery" {
		sqlStatements, err = transformationTestSelectAndCreateSupportingSQLStatementsBigquery(jobID, fs, file, project, tableName, templateData.StartDate, templateData.EndDate, templateData, dbConnection)
		if err != nil {
			logger.Fatal(project, jobID+": Cannot create SQL statements: "+fmt.Sprintf("%s", err))
		}
	} else if dbType == "postgresql" {
		sqlStatements, err = transformationTestSelectAndCreateSupportingSQLStatementsPostgresql(jobID, fs, file, project, tableName, templateData.StartDate, templateData.EndDate, templateData, dbConnection)
		if err != nil {
			logger.Fatal(project, jobID+": Cannot create SQL statements: "+fmt.Sprintf("%s", err))
		}
	} else {
		logger.Fatal(project, "Unknown dbtype: "+dbType)
	}

	templateData.InsertStatement = sqlStatements.Insert
	templateData.PreSelectHookStatement = sqlStatements.PreSelectHook
	templateData.PostSelectHookStatement = sqlStatements.PostSelectHook

	// --------------------------------------
	// Third run to really execute the created SQL statements
	// --------------------------------------

	sql, _, err := templates.Run(fs, "full", file, templateData)
	if err != nil {
		logger.SetLevel("debug")
		logger.SqlDebug(jobID, sqlStatements.Create, tableName)
		logger.FatalJob(project, jobID, fmt.Sprintf("%s: %s", jobID, err))
	}

	//If --drop the drop target table
	if vipercfg.GetBool("dropTargetTable") {
		logger.SqlDebug(jobID, sqlStatements.Drop, tableName)
		_, err = dbConnection.Exec(sqlStatements.Drop)
		if err != nil {
			logger.FatalJob(project, jobID, fmt.Sprintf("%s: %s", jobID, err))
		}
	}

	logger.SqlDebug(jobID, sqlStatements.Create, tableName)
	_, err = dbConnection.Exec(sqlStatements.Create)
	if err != nil {
		logger.FatalJob(project, jobID, fmt.Sprintf("%s: %s => %s", jobID, err, templateData.File))
	}

	if sqlStatements.Delete == "" {
		logger.SqlDebug(jobID, "/* DELETE statement empty => not executing */", tableName)
	} else {
		logger.SqlDebug(jobID, sqlStatements.Delete, tableName)
		_, err = dbConnection.Exec(sqlStatements.Delete)
		if err != nil {
			logger.FatalJob(project, jobID, fmt.Sprintf("%s: %s => %s", jobID, err, templateData.File))
		}
	}

	logger.SqlDebug(jobID, sql, tableName)

	_, err = dbConnection.Exec(sql)
	if err != nil {
		logger.FatalJob(project, jobID, fmt.Sprintf("%s: %s => %s", jobID, err, templateData.File))
	}

	logger.PrintImportantJob(jobID, "Finished")

}

func processCliVars(project string, vars string) map[string]string {
	var varsf interface{}
	err := json.Unmarshal([]byte(vars), &varsf)
	if err != nil {
		logger.Fatal(project, "vars parameter not a valid json: "+fmt.Sprintf("%s", err)+" => "+vars)
	}
	m := varsf.(map[string]interface{})

	varsMap := map[string]string{}

	for k, v := range m {
		switch vv := v.(type) {
		case string:
			fmt.Println(k, "is string", vv)
			varsMap[k] = vv
		// case float64:
		//     fmt.Println(k, "is float64", vv)
		// case []interface{}:
		//     fmt.Println(k, "is an array:")
		//     for i, u := range vv {
		//         fmt.Println(i, u)
		//     }
		default:
			logger.Fatal(project, "vars parameter: "+k+" is not a string")
		}
	}

	return varsMap
}

func findTag(searchedTags []string, searchInTags []string) bool {

	for _, tag := range searchInTags {
		for _, st := range searchedTags {
			if st == tag {
				return true
			}
		}
	}

	return false
}

// for rows.Next() {
// 	// Create a slice of interface{}'s to represent each column,
// 	// and a second slice to contain pointers to each item in the columns slice.
// 	columns := make([]interface{}, len(cols))
// 	columnPointers := make([]interface{}, len(cols))
// 	for i, _ := range columns {
// 		columnPointers[i] = &columns[i]
// 	}

// 	// Scan the result into the column pointers...
// 	if err := rows.Scan(columnPointers...); err != nil {
// 		//return err
// 	}

// 	// Create our map, and retrieve the value for each column from the pointers slice,
// 	// storing it in the map with the name of the column as the key.
// 	m := make(map[string]interface{})
// 	for i, colName := range cols {
// 		val := columnPointers[i].(*interface{})
// 		m[colName] = *val
// 	}

// 	// Outputs: map[columnName:value columnName2:value2 columnName3:value3 ...]
// 	fmt.Print(m)
// 	fmt.Println()
// }

const letterBytes = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randStringBytesRmndr(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Int63()%int64(len(letterBytes))]
	}
	return string(b)
}
