package workflow

import (
	"fmt"
	"path"
	"path/filepath"
	"strings"

	"github.com/spf13/afero"

	"gitlab.com/lesql/lesql/src/libs/templates"
)

func getTransformationConfig(file string, basepath string, modelsglobalpath string, macrospath string, project string, vars map[string]string, arrays map[string][]string, multiDimArrays2 map[string]map[string][]string, dbType string) (templates.TemplateDataConfigStruct, string, error) {
	var tableName string

	//logger.PrintImportant("Getting config: " + file)

	fileName := filepath.Base(file)
	fileBaseName := strings.TrimSuffix(fileName, ".le.sql")

	if strings.Contains(fileBaseName, ".") {
		return templates.TemplateDataConfigStruct{}, "", fmt.Errorf("no '.' is allowed in filename except in suffix '.le.sql': %s", fileName)
	}

	if strings.Contains(fileBaseName, "-") {

		/*
			aORb := regexp.MustCompile("-")
			matches := aORb.FindAllStringIndex(fileBaseName, -1)
			if len(matches) != 1 {
				logger.Fatal(project, fmt.Errorf("Only one '-' is allowed in filename: %s", fileName))
			}
		*/

		s := strings.SplitN(fileBaseName, "-", 2)
		tableName = s[1]
	} else {
		tableName = fileBaseName
	}

	templateData := templates.TemplateDataStruct{
		DbType:           dbType,
		DropTargetTable:  false,
		GlobalmodelsPath: path.Join(basepath, modelsglobalpath),
		MacrosPath:       path.Join(basepath, macrospath),
		Project:          project,
		TargetTable:      tableName,
		TargetSchema:     project,
		This:             "\"" + project + "\".\"" + tableName + "\"",
		StartDate:        "",
		EndDate:          "",
		CliVars:          map[string]string{},
		Vars:             vars,
		Arrays:           arrays,
		MultiDimArrays2:  multiDimArrays2,
	}

	var fs = afero.NewOsFs()

	_, templateDataConfig, err := templates.Run(fs, "config", file, templateData)
	if err != nil {
		return templates.TemplateDataConfigStruct{}, "", fmt.Errorf("Cannot create config: " + fmt.Sprintf("%s", err))
	}

	return templateDataConfig, tableName, nil
}
