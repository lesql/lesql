package workflow

import (
	"errors"
	"os"
	"path"
	"path/filepath"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/lesql/dag"
	"gitlab.com/lesql/lesql/src/lesqllog"
	"gitlab.com/lesql/lesql/src/libs/config"
	"gitlab.com/lesql/lesql/src/libs/model"
)

// GetWorkflow returns the workflow of a model for docs
func GetWorkflow(basepath string, modelspath string, modelsglobalpath string, macrospath string, projectID string, modelspathsuffix string, logger *lesqllog.Logger, dbType string) (*dag.Dag, error) {

	logr := logrus.New()

	loggerDag := logr.WithFields(logrus.Fields{})

	modelspath, err := config.GetConfigPath(modelspath, projectID, modelspathsuffix)
	if err != nil {
		return &dag.Dag{}, errors.New("cannot create modelsPath")
	}

	modelToRun := path.Join(basepath, modelspath)

	_, err = os.Stat(modelToRun)
	if err != nil {
		return &dag.Dag{}, nil
	}

	//logger.Println("Create DAG for project " + projectID)

	var modelFiles []string

	err = filepath.Walk(modelToRun, model.WalkThroughModelAndGetAllModelFiles(&modelFiles))
	if err != nil {
		return &dag.Dag{}, err
		//logger.Fatal(projectID, err)
	}

	jobs := make(map[string]*dag.Job)

	dagHooks := dag.Hooks{
		DagCreated: func(dag *dag.Dag) {
		},
		DagStarted: func(dag *dag.Dag) {
		},
		DagFinishedSuccess: func(dag *dag.Dag) {
		},
		DagFinishedFailed: func(dag *dag.Dag) {
		},
		JobCreated: func(dagJob *dag.Job) {
		},
		JobStarted: func(dagJob *dag.Job) {
			logrus.Infof("JobStarted: %s", dagJob.Name)
		},
		JobFinishedSuccess: func(dagJob *dag.Job) {
		},
		JobFinishedFailed: func(dagJob *dag.Job) {
		},
	}
	d, err := dag.New(projectID, loggerDag, dagHooks)

	if err != nil {
		return &dag.Dag{}, err
	}

	for _, modelFile := range modelFiles {
		logger.Debug("")
		logger.Debug("modelfile: ", modelFile)

		// First run to get vars from config
		templateDataConfig, _, err := getTransformationConfig(modelFile, basepath, modelsglobalpath, macrospath, projectID, map[string]string{}, map[string][]string{}, map[string]map[string][]string{}, dbType)
		if err != nil {
			logger.Debug("ERROR: ", err)
			return &dag.Dag{}, err
		}

		//Second run, to run the templates with correct vars
		templateDataConfig, tableName, err := getTransformationConfig(modelFile, basepath, modelsglobalpath, macrospath, projectID, templateDataConfig.Vars, templateDataConfig.Arrays, templateDataConfig.MultiDimArrays2, dbType)
		if err != nil {
			logger.Debug("ERROR: ", err)
			return &dag.Dag{}, err
		}

		logger.Debug("Generated tableName: ", tableName)

		if !d.JobExists(tableName) {
			logger.Debug("Create job: ", tableName)

			jobs[tableName], err = d.CreateJob(tableName, func() (map[string]string, error) {
				ret := make(map[string]string)
				return ret, nil
			})
			if err != nil {
				return &dag.Dag{}, err
			}
		}
		for _, reference := range templateDataConfig.Refs {
			if !d.JobExists(reference) {
				jobs[reference], err = d.CreateJob(reference, func() (map[string]string, error) {
					ret := make(map[string]string)
					return ret, nil
				})
				if err != nil {
					return &dag.Dag{}, err
				}
			}
			jobs[reference].Then(jobs[tableName])
		}

		//Needed to work on windows
		modelFile = filepath.ToSlash(modelFile)
		pathBase := filepath.ToSlash(path.Join(basepath, modelspath) + "/")

		modelFile = strings.Replace(modelFile, pathBase, "", -1)

		// fmt.Println(pathBase)
		// fmt.Println(modelFile)

		jobs[tableName].AddData("modelPath", modelFile)
		jobs[tableName].AddData("tags", strings.Join(templateDataConfig.Tags, "__|X|__"))
		jobs[tableName].AddData("tagColors", strings.Join(templateDataConfig.TagColors, "__|X|__"))
		jobs[tableName].AddData("doNotIgnoreWithoutRefs", templateDataConfig.DoNotIgnoreWithoutRefs)
	}

	return d, nil
}
