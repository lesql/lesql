package workflow

import (
	"errors"
	"os"
	"path"
	"path/filepath"

	"github.com/sirupsen/logrus"
	"gitlab.com/lesql/dag"
	"gitlab.com/lesql/lesql/src/libs/config"
	"gitlab.com/lesql/lesql/src/libs/model"
)

// CreateWorkflow returns the workflow of a model for run
func CreateWorkflow(task func(tableName string) error, basepath string, modelspath string, modelsglobalpath string, macrospath string, projectID string, modelspathsuffix string, dbType string) (*dag.Dag, error) {

	logger := logrus.New()
	loggerDag := logger.WithFields(logrus.Fields{})

	modelspath, err := config.GetConfigPath(modelspath, projectID, modelspathsuffix)
	if err != nil {
		return &dag.Dag{}, errors.New("cannot create modelsPath")
	}

	modelToRun := path.Join(basepath, modelspath)

	_, err = os.Stat(modelToRun)
	if err != nil {
		return &dag.Dag{}, nil
	}

	//logger.Println("Create DAG for project " + projectID)

	var modelFiles []string

	err = filepath.Walk(modelToRun, model.WalkThroughModelAndGetAllModelFiles(&modelFiles))
	if err != nil {
		return &dag.Dag{}, err
		//logger.Fatal(projectID, err)
	}

	jobs := make(map[string]*dag.Job)

	dagHooks := dag.Hooks{
		DagCreated: func(dag *dag.Dag) {
		},
		DagStarted: func(dag *dag.Dag) {
		},
		DagFinishedSuccess: func(dag *dag.Dag) {
		},
		DagFinishedFailed: func(dag *dag.Dag) {
		},
		JobCreated: func(dagJob *dag.Job) {
		},
		JobStarted: func(dagJob *dag.Job) {
		},
		JobFinishedSuccess: func(dagJob *dag.Job) {
		},
		JobFinishedFailed: func(dagJob *dag.Job) {
		},
	}
	d, err := dag.New(projectID, loggerDag, dagHooks)

	if err != nil {
		return &dag.Dag{}, err
		//logger.Fatal(projectID, err)
	}

	//First pass: Create jobs
	for _, modelFile := range modelFiles {

		// First run to get vars from config
		templateDataConfig, _, err := getTransformationConfig(modelFile, basepath, modelsglobalpath, macrospath, projectID, map[string]string{}, map[string][]string{}, map[string]map[string][]string{}, dbType)
		if err != nil {
			return &dag.Dag{}, err
		}

		//Second run, to run the templates with correct vars
		templateDataConfig, tableName, err := getTransformationConfig(modelFile, basepath, modelsglobalpath, macrospath, projectID, templateDataConfig.Vars, templateDataConfig.Arrays, templateDataConfig.MultiDimArrays2, dbType)
		if err != nil {
			return &dag.Dag{}, err
		}

		file := modelFile

		if !d.JobExists(tableName) {
			jobs[tableName], err = d.CreateJob(tableName, func() (map[string]string, error) {
				task(file)
				ret := make(map[string]string)
				return ret, nil
			})

			if err != nil {
				return &dag.Dag{}, err
				//logger.Fatal(projectID, err)
			}
		}

	}

	//Second pass: Create edges
	for _, modelFile := range modelFiles {

		// First run to get vars from config
		templateDataConfig, _, err := getTransformationConfig(modelFile, basepath, modelsglobalpath, macrospath, projectID, map[string]string{}, map[string][]string{}, map[string]map[string][]string{}, dbType)
		if err != nil {
			return &dag.Dag{}, err
		}

		//Second run, to run the templates with correct vars
		templateDataConfig, tableName, err := getTransformationConfig(modelFile, basepath, modelsglobalpath, macrospath, projectID, templateDataConfig.Vars, templateDataConfig.Arrays, templateDataConfig.MultiDimArrays2, dbType)
		if err != nil {
			return &dag.Dag{}, err
		}
		for _, reference := range templateDataConfig.Refs {

			// Do not add an edge if reference job does not exists. In this case the ref is a source table
			if _, ok := jobs[reference]; ok {
				jobs[reference].Then(jobs[tableName])
			}
		}

	}

	return d, nil
}
