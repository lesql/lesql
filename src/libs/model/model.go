package model

import (
	"os"
	"path/filepath"
)

func WalkThroughModelAndGetAllModelFiles(files *[]string) filepath.WalkFunc {
	return func(path string, f os.FileInfo, err error) error {
		if f.Mode().IsDir() {
			return nil
		}
		*files = append(*files, path)
		return nil
	}
}
