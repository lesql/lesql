package config

import (
	"bytes"
	"path"
)

type modelspathTemplData struct {
	ProjectName string
}

var modelspathByte bytes.Buffer

//GetConfigPath creates models path from confg and projectID
func GetConfigPath(modelspath string, projectID string, modelspathsuffix string) (string, error) {

	// //Render path template
	// t, err := template.New("modelspath").Parse(modelspathTempl)
	// modelspathTemplData := modelspathTemplData{
	// 	ProjectName: projectID,
	// }
	// if err != nil {
	// 	return "", err
	// }
	// err = t.Execute(&modelspathByte, modelspathTemplData)
	// if err != nil {
	// 	return "", nil
	// }
	// modelspath := fmt.Sprintf("%s", &modelspathByte)

	return path.Join(modelspath, projectID, modelspathsuffix), nil
}
