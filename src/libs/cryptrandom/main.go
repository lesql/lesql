package cryptrandom

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"

	"github.com/gtank/cryptopasta"
)

//CreateRandom returns a random string with length n
func CreateRandom(n int) (string, error) {
	bytes := make([]byte, n)
	_, err := rand.Read(bytes)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return "", err
	}

	const letters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
	for i, b := range bytes {
		bytes[i] = letters[b%byte(len(letters))]
	}
	return string(bytes), nil

}

func EncryptString(yamlMasterKey string, yamlKey string, content string) (string, error) {
	keyHash := cryptopasta.Hash(yamlMasterKey, []byte(yamlKey))
	var key32 [32]byte
	copy(key32[:], keyHash)
	//Encrypt

	ciphertext, _ := cryptopasta.Encrypt([]byte(content), &key32)
	encoded := base64.StdEncoding.EncodeToString([]byte(ciphertext))

	return fmt.Sprintf("%s", encoded), nil
}
