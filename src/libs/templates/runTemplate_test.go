package templates

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"path"
	"path/filepath"
	"reflect"
	"strings"
	"testing"

	"github.com/spf13/afero"
	"github.com/spf13/viper"
	"gotest.tools/assert"
)

var update = flag.Bool("update", false, "update golden file")

// getGoldenFile returns the golden file content. If the `update` is specified, it updates the
// file with the current output and returns it.
func getGoldenFile(t *testing.T, actual []byte, fileName string) []byte {
	golden := filepath.Join("testdata", fileName+".golden")
	if *update {
		if err := ioutil.WriteFile(golden, actual, 0644); err != nil {
			t.Fatalf("Error writing golden file for filename=%s: %s", fileName, err)
		}
	}
	expected, err := ioutil.ReadFile(golden)
	if err != nil {
		t.Fatal(err)
	}
	return expected
}

func getTemplateDataStructure() TemplateDataStruct {
	project := "zzzdummy"
	tableName := "test"
	startDate := "2019-01-01"
	endDate := "2019-01-31"
	dbType := "postgresql"

	templateData := TemplateDataStruct{
		DbType:          dbType,
		DropTargetTable: false,
		Project:         project,
		TargetTable:     tableName,
		TargetSchema:    project,
		This:            "\"" + project + "\".\"" + tableName + "\"",
		StartDate:       startDate,
		EndDate:         endDate,
		CliVars:         map[string]string{},
		Vars:            map[string]string{},
		Arrays:          map[string][]string{},
		MultiDimArrays2: map[string]map[string][]string{},
		Tags:            []string{},
		TagColors:       []string{},
	}

	return templateData
}

func getViperConfig() *viper.Viper {
	//Create config
	vipercfgx := viper.New()
	vipercfgx.Set("debug", false)
	vipercfgx.Set("basepath", "testdata/templates/")
	vipercfgx.Set("macrospath", "macros")
	vipercfgx.Set("modelsglobalpath", "models_global")
	vipercfgx.Set("modelspath", "models")

	return vipercfgx
}

func TestRunTemplate(t *testing.T) {
	cases := []struct {
		id           string
		responseType string
		file         string
	}{
		{
			"0101Select",
			"select",
			"/testdata/templates/models/zzzdummy/0101-test.le.sql",
		},
		{
			"0101PreSelectHook",
			"preSelectHook",
			"/testdata/templates/models/zzzdummy/0101-test.le.sql",
		},
		{
			"PostSelectHook0101",
			"postSelectHook",
			"/testdata/templates/models/zzzdummy/0101-test.le.sql",
		},
		{
			"0101Full",
			"full",
			"/testdata/templates/models/zzzdummy/0101-test.le.sql",
		},

		{
			"0102Full",
			"full",
			"/testdata/templates/models/zzzdummy/0102-test_materialized.le.sql",
		},

		{
			"0103Full",
			"full",
			"/testdata/templates/models/zzzdummy/0103-test_global_model_and_macros.le.sql",
		},

		{
			"0104Config",
			"config",
			"/testdata/templates/models/zzzdummy/0104-test_config.le.sql",
		},

		{
			"0305Config",
			"config",
			"/testdata/templates/models/zzzdummy/0305-test_vars_and_arrays.le.sql",
		},
		{
			"0305Select",
			"select",
			"/testdata/templates/models/zzzdummy/0305-test_vars_and_arrays.le.sql",
		},
		{
			"0305Full",
			"full",
			"/testdata/templates/models/zzzdummy/0305-test_vars_and_arrays.le.sql",
		},
	}

	for _, c := range cases {
		vipercfg := getViperConfig()
		templateData := getTemplateDataStructure()

		templateData.GlobalmodelsPath = path.Join(vipercfg.GetString("basepath"), vipercfg.GetString("modelsglobalpath"))
		templateData.MacrosPath = path.Join(vipercfg.GetString("basepath"), vipercfg.GetString("macrospath"))

		var fs = afero.NewOsFs()

		path, _ := filepath.Abs(filepath.Dir("."))
		file := filepath.Join(path, c.file)

		// t.Errorf(fmt.Sprintf("%v", generatedConfig))

		var goldenBytes []byte
		var stringToTest string
		var sqlText string
		var templateDataConfig TemplateDataConfigStruct
		var err error

		if c.responseType == "config" {

			_, templateDataConfig, err = Run(fs, c.responseType, file, templateData)
			if err != nil {
				t.Errorf("Cannot create template: %s", err)
			}
			generatedConfigJson, err := json.MarshalIndent(templateDataConfig, "", "  ")
			if err != nil {
				t.Errorf("Cannot marshal generatedConfig: %s", err)
			}

			stringToTest = fmt.Sprintf("%s", generatedConfigJson)

			goldenBytes = getGoldenFile(t, generatedConfigJson, t.Name()+c.id)

		} else if c.responseType == "full" {
			// -----------------------------
			// 1) First get the config
			// -----------------------------

			_, templateDataConfig, err = Run(fs, "config", file, templateData)
			if err != nil {
				t.Errorf("Cannot create template: %s", err)
			}

			// -----------------------------
			// 2) Create template with config data
			// -----------------------------

			templateData = CreateTemplateDataStruct(templateData, templateDataConfig)

			sqlText, templateDataConfig, err = Run(fs, c.responseType, file, templateData)
			if err != nil {
				t.Errorf("Cannot create template: %s", err)
			}

			goldenBytes = getGoldenFile(t, []byte(sqlText), t.Name()+c.id)
			stringToTest = sqlText

		} else if c.responseType == "select" || c.responseType == "preSelectHook" || c.responseType == "postSelectHook" {

			sqlText, templateDataConfig, err = Run(fs, c.responseType, file, templateData)
			if err != nil {
				t.Errorf("Cannot create template: %s", err)
			}

			goldenBytes = getGoldenFile(t, []byte(sqlText), t.Name()+c.id)
			stringToTest = sqlText
		} else {
			t.Errorf("Unknown responseType: " + c.responseType)
		}

		if !reflect.DeepEqual(goldenBytes, []byte(stringToTest)) {

			t.Errorf("Expected golden file '%s' to match output\n----------------------------\n%s----------------------------\nBut it should\n----------------------------\n%s----------------------------\n", t.Name()+c.id, stringToTest, goldenBytes)
			goldenBytes := string(goldenBytes)
			assert.Equal(t, goldenBytes, stringToTest)

		}
	}
}

func TestRunTemplateErrors(t *testing.T) {
	cases := []struct {
		id           string
		responseType string
		file         string
		message      string
	}{
		{
			"ErrorFile",
			"full",
			"not_existing",
			"Load: open ",
		},
		{
			"ErrorResponseType",
			"",
			"/testdata/templates/models/zzzdummy/0101-test.le.sql",
			"unknown responseType given",
		},
		{
			"ErrorMaterialized",
			"full",
			"/testdata/templates/models/zzzdummy/0201-test_materialized_error.le.sql",
			"error calling materialized: 'materialized' has to be 'table' or 'view'",
		},
	}

	for _, c := range cases {
		vipercfg := getViperConfig()
		templateData := getTemplateDataStructure()

		templateData.GlobalmodelsPath = path.Join(vipercfg.GetString("basepath"), vipercfg.GetString("modelsglobalpath"))
		templateData.MacrosPath = path.Join(vipercfg.GetString("basepath"), vipercfg.GetString("macrospath"))

		var fs = afero.NewOsFs()

		path, _ := filepath.Abs(filepath.Dir("."))
		file := filepath.Join(path, c.file)

		_, _, err := Run(fs, c.responseType, file, templateData)
		if !strings.Contains(err.Error(), c.message) {
			t.Errorf("Expected error to contain '%s' got '%s'", c.message, err.Error())
		}
	}
}
