package templates

type TemplateDataConfigStruct struct {
	Delete                 bool
	Materialized           string
	DoNotIgnoreWithoutRefs string
	PrimaryColumns         []string
	PartitionColumn        string
	Tags                   []string
	TagColors              []string

	TimestampColumn string
	Vars            map[string]string
	Arrays          map[string][]string
	MultiDimArrays2 map[string]map[string][]string
	Refs            []string
}

type TemplateDataStruct struct {
	File                    string
	DbType                  string
	DropTargetTable         bool
	EndDate                 string
	GlobalmodelsPath        string
	MacrosPath              string
	Project                 string
	StartDate               string
	TargetTable             string
	TargetSchema            string
	This                    string
	CreateTableStatement    string
	DeleteStatement         string
	InsertStatement         string
	PreSelectHookStatement  string
	PostSelectHookStatement string
	Config                  TemplateDataConfigStruct
	CliVars                 map[string]string
	Vars                    map[string]string
	Arrays                  map[string][]string
	Tags                    []string
	TagColors               []string
	MultiDimArrays2         map[string]map[string][]string
}
