package templates

var templatesMapPostgresql = map[string]string{
	"config":         fullTemplatePostgresql,
	"select":         selectOnlyTemplatePostgresql,
	"full":           fullTemplatePostgresql,
	"preSelectHook":  preSelectHookTemplatePostgresql,
	"postSelectHook": postSelectHookTemplatePostgresql,
}

const fullTemplatePostgresql = `{{block "CONFIG" .}}{{end}}
{{.PreSelectHookStatement}}

{{block "PRE_SELECT_WITH_HOOKS" . -}}/* No pre_select_with_hooks */{{- end}}
{{.InsertStatement}}

{{- block "SELECT" . -}}{{- end}};

{{define "ONCONFLICT"}}ON CONFLICT
{{ template "ONCONFLICT_TARGET" . }}
{{ template "ONCONFLICT_ACTION" . }}
{{end}}

{{.PostSelectHookStatement}}
`

//{{/*Materialized {{.Config.Materialized}} */}}

const selectOnlyTemplatePostgresql = `{{block "PRE_SELECT_WITH_HOOKS" . -}}/* No pre_select_with_hooks */{{- end}}

{{block "SELECT" .}}{{end}}
{{define "ONCONFLICT"}}{{end}}`

const preSelectHookTemplatePostgresql = `{{block "PRE_SELECT_HOOKS" . -}}/* No pre_select_hooks */{{- end}}`
const postSelectHookTemplatePostgresql = `{{block "POST_SELECT_HOOKS" . -}}/* No post_select_hooks */{{- end}}`
