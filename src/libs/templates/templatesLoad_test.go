package templates

import (
	"path"
	"testing"
	"text/template"

	"github.com/spf13/afero"
)

func TestLoadModelTemplates(t *testing.T) {

	modelsPath := "/models"
	modelsPathSubdir := path.Join(modelsPath, "subdir")

	var tplFiles = []struct {
		file    string
		content string
	}{
		{path.Join(modelsPathSubdir, "test1.tpl"), "This is a template named test1.tpl"},
		{path.Join(modelsPath, "test2.tpl"), "This is a template named test2.tpl"},
	}

	//Prepare model template file structure
	var fs = afero.NewMemMapFs()
	fs.Mkdir(modelsPath, 0755)
	fs.Mkdir(modelsPathSubdir, 0755)

	for _, tplFile := range tplFiles {
		err := afero.WriteFile(fs, tplFile.file, []byte(tplFile.content), 0644)
		if err != nil {
			t.Errorf("Cannot write model templates (afero error?): %s -> '%s'", tplFile.file, err)
		}
	}

	tpl := template.New("test template")
	_, err := loadModelTemplates(
		fs,
		modelsPath,
		tpl,
		template.FuncMap{},
	)

	if err != nil {
		t.Errorf("Cannot load model templates: '%s'", err)
	}
}
