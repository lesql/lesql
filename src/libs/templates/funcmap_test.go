package templates

import (
	"strconv"
	"testing"
	"text/template"

	"github.com/stretchr/testify/assert"
)

func TestGetFuncmap(t *testing.T) {
	tmpl := template.New("test funcmap")
	tmplDataConfig := TemplateDataConfigStruct{}
	funcmap := getFuncmap(tmpl, &tmplDataConfig)
	assert.IsType(t, template.FuncMap{}, funcmap)
}

func TestFuncmapRef(t *testing.T) {
	fmref := funcmapRef("testref")
	if fmref != "testref" {
		t.Error("Value incorrect, got: ", fmref, ", want: testref")
	}
}

func TestFuncmapDelimit(t *testing.T) {
	fmdel := funcmapDelimit([]string{"test", "test2", "test3"}, ",")
	if fmdel != "test,test2,test3" {
		t.Error("Value incorrect, got: '", fmdel, "', want: ' test,test2,test3 '")
	}
}

func TestFuncmapDefault(t *testing.T) {
	cases := []struct {
		arg    interface{}
		value  interface{}
		result interface{}
	}{
		{
			"test_case_nil",
			nil,
			"test_case_nil",
		},
		{
			"test_case_len0",
			"",
			"test_case_len0",
		},
		{
			"test_case_bool",
			true,
			true,
		},
		{
			"test_case_default",
			123.123,
			123.123,
		},
		{
			"test_default_return",
			"returning",
			"returning",
		},
	}

	for idx, c := range cases {
		t.Run(t.Name()+strconv.Itoa(idx), func(t *testing.T) {
			val := funcmapDefault(c.arg, c.value)
			if val != c.result {
				t.Errorf("Value incorrect, got: %d, want: %d", val, c.result)
			}
		})
	}
}
