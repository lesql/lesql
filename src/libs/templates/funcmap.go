package templates

import (
	"bytes"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"text/template"
)

func funcmapDefault(arg interface{}, value interface{}) interface{} {
	//defer recovery()
	if value == nil {
		return arg
	}
	v := reflect.ValueOf(value)
	switch v.Kind() {
	case reflect.String, reflect.Slice, reflect.Array, reflect.Map:
		if v.Len() == 0 {
			return arg
		}
	case reflect.Bool:
		if !v.Bool() {
			return arg
		}
	default:
		fmt.Println(v.Kind())
		return value
	}

	return value
}

//This just gives the string back
//For the config run, there is an extra funcmap for ref
func funcmapRef(str string) string {
	return str
}

func funcmapDelimit(values []string, delimiter string) string {
	return strings.Join(values, delimiter)
}

func getFuncmap(t *template.Template, templateDataConfig *TemplateDataConfigStruct) template.FuncMap {
	funcMap := template.FuncMap{
		"default": funcmapDefault,
		"delimit": funcmapDelimit,
		"doNotIgnoreWithoutRefs": func() (string, error) {
			templateDataConfig.DoNotIgnoreWithoutRefs = "true"
			return "", nil
		},
		"globalmodel": func(str string, context interface{}) (string, error) {
			buf := bytes.NewBuffer([]byte{})
			t.ExecuteTemplate(buf, str, context)
			if len(buf.String()) == 0 {

				return "", errors.New("Globalmodel does not exist or empty: " + str)
			}
			return buf.String(), nil
		},

		"macro": func(str string, context interface{}) (string, error) {
			buf := bytes.NewBuffer([]byte{})
			t.ExecuteTemplate(buf, str, context)
			if len(buf.String()) == 0 {

				return "", errors.New("Globalmodel does not exist or empty: " + str)
			}
			return buf.String(), nil
		},
		"materialized": func(value string) (string, error) {
			if (value != "table") && (value != "view") {
				return "", errors.New("'materialized' has to be 'table' or 'view'")
			}
			templateDataConfig.Materialized = value
			return "", nil
		},
		"primary": func(primaryColumns ...string) string {
			if logger != nil {
				logger.Warningln("DEPRECATION WARNING: Do not use 'primary' anymore. Please use 'setPrimaryKey' instead")
			}
			templateDataConfig.PrimaryColumns = primaryColumns
			return ""
		},
		"ref": func(str string) string {
			templateDataConfig.Refs = append(templateDataConfig.Refs, str)
			return str
		},
		"noDelete": func(noDelete string) string {
			if noDelete == "true" {
				templateDataConfig.Delete = false
			} else {
				templateDataConfig.Delete = true
			}
			return ""
		},
		"setPartitionColumn": func(partitionColumn string) string {
			templateDataConfig.PartitionColumn = partitionColumn
			return ""
		},
		"setPrimaryKey": func(primaryColumns ...string) string {
			templateDataConfig.PrimaryColumns = primaryColumns
			return ""
		},
		"setTags": func(tags ...string) string {
			templateDataConfig.Tags = tags
			return ""
		},
		"setTagColors": func(tagColors ...string) string {
			templateDataConfig.TagColors = tagColors
			return ""
		},
		"setTimestampColumn": func(columnName string) string {
			templateDataConfig.TimestampColumn = columnName
			return ""
		},
		"setvar": func(key string, value string) string {
			templateDataConfig.Vars[key] = value
			return ""
		},
		"setarray": func(key string, args ...string) string {
			//fmt.Printf("%+v\n", args)
			templateDataConfig.Arrays[key] = args
			return ""
		},
		"setMultiDimArray2": func(key string, args ...string) string {
			/*
				setMultiDimArray2 creates a 2 dimensional array
				setMultiDimArray2("arrayName" "x", "1", "y", "2", "x", "3") creates a map[x:[1 3] y:[2]] called "arrayName"
			*/
			multiDimArray := make(map[string][]string)
			for index, value := range args {
				if (index % 2) != 0 {
					continue
				}
				multiDimArray[value] = append(multiDimArray[value], args[index+1])
			}
			templateDataConfig.MultiDimArrays2[key] = multiDimArray
			return ""
		},
	}

	return funcMap
}
