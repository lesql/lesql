SELECT 
  CONCAT(column1, ' ', 'Du', ' ', '{{ .Vars.bla | default "bla" }}' )::TEXT as column1, 
  CONCAT(column1, ' ', 'Du', ' ', '{{ .CliVars.channel | default "bla" }}' )::TEXT as hello, 
  "date",
  1::INT8 as blub, 
  1.1::DECIMAL(12,4) as "bla",
  NOW() as update_ts
{{/*ROM zzzdummy."test1"*/}}
FROM "{{.Project}}".{{ ref "test1" }}
WHERE "date" BETWEEN '{{ .StartDate }}' and '{{ .EndDate }}'