case
  when (contains({{ . }}, 'osx')) then 'osx'
  when (contains({{ . }}, 'android')) then 'android'
  when (contains({{ . }}, 'ios')) then 'ios'
  else 'other'
end as renamed_product
