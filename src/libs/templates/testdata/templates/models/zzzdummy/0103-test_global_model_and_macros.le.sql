{{block "CONFIG" . }}
  {{ setvar "bla" "blub"}}
  {{ noDelete "true" }}
{{end}}

{{block "SELECT" .}}
{{ globalmodel "test/test_blub" . }}
{{end}}

{{ block "POST_SELECT_HOOKS" . }}
{{ macro "zzz-test-macros/rename_category" "Bla" }}
{{ macro "zzz-test-macros/blub/rename_category" "Blub" }}
{{ globalmodel "test/test_blub_update" . }}
{{end}}