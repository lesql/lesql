{{ block "CONFIG" .}}
  {{ materialized "asdf" }}
  {{ setPrimaryKey "hello"}}
{{end}}

{{ block "SELECT" .}}
SELECT
  column1::TEXT,
  CONCAT(column1, ' ', '{{ .CliVars.channel | default "blub" }}')::TEXT as "hello",
  a."date" as date,
  1::INT8 as blub,
  1.1::DECIMAL(12,4) as bla,
  NOW() as update_ts,
  b.column_a
FROM {{ .Project }}.{{ ref "test1" }} as a
left join abc as b
on a.date = b.date_order
WHERE a."date" BETWEEN '{{ .StartDate }}' and '{{ .EndDate }}'
{{end}}
