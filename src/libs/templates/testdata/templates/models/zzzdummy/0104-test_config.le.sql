{{ block "CONFIG" .}}
  {{/*materialized "table"*/}}
  {{ setPrimaryKey "foo-bar" "date2"}}
  {{ setTimestampColumn "date2"}}
  {{ setvar "ttvar" "ttvalue"}}
  {{ setarray "value1" "value2" "value3"}}
{{end}}

{{ block "SELECT" .}}
SELECT
  column1::TEXT,
  CONCAT(column1, ' ', '{{ .CliVars.channel | default "blub" }}')::TEXT as "foo-bar",
  a."date" as date2,
  1::INT8 as blub,
  1.1::DECIMAL(12,4) as bla,
  NOW() as update_ts,
  b.column_a::TEXT
FROM {{ .Project }}.{{ ref "test1" }} as a
left join abc as b
on a.date = b.date
WHERE a."date" BETWEEN '{{ .StartDate }}' and '{{ .EndDate }}'
{{ template "ONCONFLICT" . }}
{{end}}

{{define "ONCONFLICT_TARGET"}}ON CONSTRAINT "zzzdummy_test_pkey"{{end}}
{{define "ONCONFLICT_ACTION"}}DO NOTHING{{end}}

{{ block "POST_SELECT_HOOKS" . }}
UPDATE {{ .This }}
SET blub=13
WHERE "date2" BETWEEN '{{ .StartDate }}' and '{{ .EndDate }}'
{{end}}


{{ block "PRE_SELECT_HOOKS" . }}
UPDATE {{ .This }}
SET blub=14
WHERE "date2" BETWEEN '{{ .StartDate }}' and '{{ .EndDate }}'
{{end}}

{{ block "PRE_SELECT_WITH_HOOKS" . }}
with abc as (SELECT 'a' as column_a, NOW()::date as date)
{{end}}
