{{ block "CONFIG" .}}
  {{ noDelete "false" }}
  {{ materialized "table" }}
  {{ setPrimaryKey "hello" "date_order"}}
  {{ setTimestampColumn "date_order"}}
{{end}}

{{ block "SELECT" .}}
SELECT
  column1::TEXT,
  CONCAT(column1, ' ', '{{ .CliVars.channel | default "blub" }}')::TEXT as "hello",
  a."date" as date_order,
  1::INT8 as blub,
  1.1::DECIMAL(12,4) as bla,
  NOW() as update_ts,
  b.column_a
FROM {{ .Project }}.{{ ref "test1" }} as a
left join abc as b
on a.date = b.date_order
WHERE a."date" BETWEEN '{{ .StartDate }}' and '{{ .EndDate }}'
{{end}}

{{ block "POST_SELECT_HOOKS" . }}
UPDATE {{ .This }}
SET blub=13
WHERE "date_order" BETWEEN '{{ .StartDate }}' and '{{ .EndDate }}'
{{end}}


{{ block "PRE_SELECT_HOOKS" . }}
UPDATE {{ .This }}
SET blub=14
WHERE "date_order" BETWEEN '{{ .StartDate }}' and '{{ .EndDate }}'
{{end}}

{{ block "PRE_SELECT_WITH_HOOKS" . }}
with abc as (SELECT 'a' as column_a, NOW()::date as date_order)
{{end}}
