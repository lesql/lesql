{{ block "CONFIG" .}}
  {{setvar "varName" "x"}}
  {{setarray "arrayName" "A" "B" "C"}}
  {{setMultiDimArray2 "multiDimArray2Name" "keyName" "A" "keyName" "B" "keyName" "C" "keyName2" "L" "keyName2" "M" "keyName2" "N"}}
{{end}}

{{ block "SELECT" .}}

SELECT
{{ range $index, $element := .Arrays.arrayName}}
  "{{ $element}}",
{{- end}}
{{ range $index, $element := .MultiDimArrays2.multiDimArray2Name}}
  {{ range $index2, $element2 := $element}}
  "{{$index}}_index{{ $index2 }}_value{{$element2}}",
  {{- end}}
{{- end}}
  "{{ .Vars.varName }}"

FROM {{ .Project }}.{{ ref "test1" }} as a
WHERE a."date" BETWEEN '{{ .StartDate }}' and '{{ .EndDate }}'

{{end}}

