{{ block "SELECT" .}}
SELECT 
  *,
  NOW() as update_ts
FROM zzzdummy.{{ ref "test1" }}
WHERE "date" BETWEEN '{{ .StartDate }}' and '{{ .EndDate }}'
{{end}}

{{/* macro "zzz-test-macros/rename_category" "Bla" */}}
{{/* macro "zzz-test-macros/blub/rename_category" "Blub" */}}
