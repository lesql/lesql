package templates

var templatesMapBigquery = map[string]string{
	"config":         fullTemplateBigquery,
	"select":         selectOnlyTemplateBigquery,
	"full":           fullTemplateBigquery,
	"preSelectHook":  preSelectHookTemplateBigquery,
	"postSelectHook": postSelectHookTemplateBigquery,
}

const fullTemplateBigquery = `{{block "CONFIG" .}}{{end}}
{{.PreSelectHookStatement}}

{{block "PRE_SELECT_WITH_HOOKS" . -}}/* No pre_select_with_hooks */{{- end}}
{{.InsertStatement}}

{{- block "SELECT" . -}}{{- end}};

{{define "ONCONFLICT"}}ON CONFLICT
{{ template "ONCONFLICT_TARGET" . }}
{{ template "ONCONFLICT_ACTION" . }}
{{end}}

{{.PostSelectHookStatement}}
`

//{{/*Materialized {{.Config.Materialized}} */}}

const selectOnlyTemplateBigquery = `{{block "PRE_SELECT_WITH_HOOKS" . -}}/* No pre_select_with_hooks */{{- end}}

{{block "SELECT" .}}{{end}}
{{define "ONCONFLICT"}}{{end}}`

const preSelectHookTemplateBigquery = `{{block "PRE_SELECT_HOOKS" . -}}/* No pre_select_hooks */{{- end}}`
const postSelectHookTemplateBigquery = `{{block "POST_SELECT_HOOKS" . -}}/* No post_select_hooks */{{- end}}`
