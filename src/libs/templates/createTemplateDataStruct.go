package templates

func CreateTemplateDataStruct(templateData TemplateDataStruct, templateDataConfig TemplateDataConfigStruct) TemplateDataStruct {

	templateData.Config = templateDataConfig

	templateData.Vars = templateDataConfig.Vars
	templateDataConfig.Vars = map[string]string{}

	templateData.Arrays = templateDataConfig.Arrays
	templateDataConfig.Arrays = map[string][]string{}

	templateData.MultiDimArrays2 = templateDataConfig.MultiDimArrays2
	templateDataConfig.MultiDimArrays2 = map[string]map[string][]string{}

	templateData.Tags = templateDataConfig.Tags
	templateDataConfig.Tags = []string{}

	templateData.TagColors = templateDataConfig.TagColors
	templateDataConfig.TagColors = []string{}

	return templateData
}
