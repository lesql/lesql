package templates

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/fatih/color"
	"github.com/spf13/afero"
	"gitlab.com/lesql/lesql/src/lesqllog"
)

var logger *lesqllog.Logger

func Run(fs afero.Fs, responseType string, file string, templateData TemplateDataStruct) (string, TemplateDataConfigStruct, error) {

	var (
		tpl bytes.Buffer
	)

	var templatesMap = map[string]string{}

	if templateData.DbType == "bigquery" {
		if templatesMapBigquery[responseType] == "" {
			return "", TemplateDataConfigStruct{}, fmt.Errorf("unknown responseType given %s", responseType)
		}
		templatesMap = templatesMapBigquery
	} else if templateData.DbType == "postgresql" {
		if templatesMapPostgresql[responseType] == "" {
			return "", TemplateDataConfigStruct{}, fmt.Errorf("unknown responseType given %s", responseType)
		}
		templatesMap = templatesMapPostgresql
	} else {
		return "", TemplateDataConfigStruct{}, fmt.Errorf("unknown dbType given %s", templateData.DbType)

	}

	templateDataConfig := initRunTemplateDataConfig(responseType)

	fileName := filepath.Base(file)
	name := strings.TrimSuffix(fileName, ".sql")

	t := template.New(name)

	funcMap := getFuncmap(t, &templateDataConfig)

	//Parse Macros
	t, err := loadModelTemplates(fs, templateData.MacrosPath, t, template.FuncMap{})
	if err != nil {
		color.Red(fmt.Sprintf("Parse macros: %s", err))
		return "", TemplateDataConfigStruct{}, err
	}

	//Parse global models
	t, err = loadModelTemplates(
		fs,
		templateData.GlobalmodelsPath,
		t,
		template.FuncMap{
			"default": funcmapDefault,
			"delimit": funcmapDelimit,
			"ref": func(str string) string {
				templateDataConfig.Refs = append(templateDataConfig.Refs, str)
				return str
			},
		},
	)
	if err != nil {
		color.Red(fmt.Sprintf("Parse globalmodel: %s", err))
		os.Exit(1)
		return "", TemplateDataConfigStruct{}, err
	}

	t.Funcs(funcMap)

	t, err = t.Parse(templatesMap[responseType])
	if err != nil {
		color.Red(fmt.Sprintf("Parse: %s", err))
		return "", TemplateDataConfigStruct{}, err
	}

	afs := &afero.Afero{Fs: fs}
	fileByteString, err := afs.ReadFile(file)
	if err != nil {
		color.Red(fmt.Sprintf("Load: %s", err))
		return "", TemplateDataConfigStruct{}, fmt.Errorf("Load: %s", err)
	}

	t2, err := t.New(fileName).Parse(string(fileByteString))
	if err != nil {
		color.Red(fmt.Sprintf("New Parse: %s", err))
		return "", TemplateDataConfigStruct{}, fmt.Errorf("Load: %s", err)
	}

	//See #13 how Parse + AddParseTree can work like ParseFiles
	t, err = t2.AddParseTree(fileName, t.Tree)
	//t, err = t.ParseFiles(file)
	if err != nil {
		return "", TemplateDataConfigStruct{}, fmt.Errorf("Parse tree: %s", err)
	}
	// fmt.Println("==========")
	// for _, tpl := range t.Templates() {
	// 	fmt.Println(tpl.Name())
	// }
	// fmt.Println("+++++")
	// fmt.Println(t.Name())

	if err := t.Execute(&tpl, templateData); err != nil {
		return "", TemplateDataConfigStruct{}, fmt.Errorf("Execute template: %s", err)
	}

	if responseType == "config" {
		return "", templateDataConfig, nil
	}

	return tpl.String(), TemplateDataConfigStruct{}, nil

}

func initRunTemplateDataConfig(responseType string) TemplateDataConfigStruct {
	var templateDataConfig TemplateDataConfigStruct

	templateDataConfig.Delete = true
	templateDataConfig.TimestampColumn = "date"
	templateDataConfig.Vars = map[string]string{}
	templateDataConfig.Arrays = map[string][]string{}
	templateDataConfig.MultiDimArrays2 = map[string]map[string][]string{}

	if responseType == "config" {
		templateDataConfig.Materialized = "view"
	}

	return templateDataConfig
}
