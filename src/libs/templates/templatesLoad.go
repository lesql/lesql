package templates

import (
	"errors"
	"os"
	"path"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/spf13/afero"
)

// loadModelTemplates are needed, to give every template a proper name
// ParseFile does not work for this:
// > When parsing multiple files with the same name in different directories, the last
// > one mentioned will be the one that results. For instance, ParseFiles("a/foo", "b/foo")
// > stores "b/foo" as the template named "foo", while "a/foo" is unavailable.
func loadModelTemplates(fs afero.Fs, templatePath string, t *template.Template, funcMap map[string]interface{}) (*template.Template, error) {
	afs := &afero.Afero{Fs: fs}

	if exists, _ := afs.DirExists(templatePath); !exists {
		return nil, errors.New("Path does not exist: " + templatePath)
	}
	err := afs.Walk(templatePath, loadModelTemplate(fs, templatePath, t, funcMap))
	return t, err
}

func loadModelTemplate(fs afero.Fs, templatePath string, t *template.Template, funcMap map[string]interface{}) filepath.WalkFunc {
	return func(file string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			templatePath = path.Join(templatePath)

			// color.Green("Path:      " + file)
			// color.Green("templatePath: " + templatePath)

			//Create proper template name
			name := strings.TrimPrefix(filepath.ToSlash(file), filepath.ToSlash(templatePath)+"/") //filepath.ToSlash() is needed to work on Windows
			name = strings.TrimSuffix(name, ".le.sql")

			afs := &afero.Afero{Fs: fs}
			dat, err := afs.ReadFile(file)
			if err != nil {
				return err
			}

			test, err := template.New(name).Funcs(funcMap).Parse(string(dat))
			if err != nil {
				return err
			}
			t, err = t.AddParseTree(name, test.Tree)
			// color.Red(test.Name())
			if err != nil {
				return err
			}
		}
		return nil
	}
}
