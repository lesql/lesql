package dbprovider

import (
	"database/sql"
	"errors"
	"sync"

	"gitlab.com/lesql/lesql/src/lesqllog"
	"gitlab.com/lesql/lesql/src/libs/cfgprovider"
)

type Driver interface {
	GetDbConnection(projectname string, settings cfgprovider.DatabaseCredentials, logger *lesqllog.Logger) (*sql.DB, error)
}

var (
	driversMu sync.RWMutex
	drivers   = make(map[string]Driver)
)

//Register registers a cfgprovider. Run it from init() in cfgprovider plugin
func Register(name string, driver Driver) {
	driversMu.Lock()

	defer driversMu.Unlock()

	if driver == nil {
		panic("dbprovider: Register driver is nil")
	}

	if _, dup := drivers[name]; dup {
		panic("dbprovider: Register called twice for driver " + name)
	}

	drivers[name] = driver

}

//GetDbConnection gets the GetDbConnection
func GetDbConnection(name string, projectName string, settings cfgprovider.DatabaseCredentials, logger *lesqllog.Logger) (*sql.DB, error) {

	logger.Debug("GetDbConnection from dbprovider: " + name)

	if drivers[name] == nil {
		return nil, errors.New("Unknown driver: " + name)
	}

	databaseConnection, err := drivers[name].GetDbConnection(projectName, settings, logger)
	if err != nil {
		return nil, err
	}

	return databaseConnection, nil
}

//DbProviderExists tests if a specific DbProvider was registered
func DbProviderExists(name string) bool {

	if _, ok := drivers[name]; ok {
		return true
	}

	return false
}
