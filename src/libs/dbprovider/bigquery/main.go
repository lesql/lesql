package bigquery

import (
	"database/sql"
	"encoding/base64"
	"fmt"

	_ "github.com/solcates/go-sql-bigquery"

	"gitlab.com/lesql/lesql/src/lesqllog"
	"gitlab.com/lesql/lesql/src/libs/cfgprovider"
	"gitlab.com/lesql/lesql/src/libs/dbprovider"
)

// Driver is ...
type Driver struct{}

// GetDbConnection opens a new connection to the database. name is a connection string.
func (d *Driver) GetDbConnection(projectname string, settings cfgprovider.DatabaseCredentials, logger *lesqllog.Logger) (*sql.DB, error) {
	return getDbConnection(settings, logger)
}

func init() {
	dbprovider.Register("bigquery", &Driver{})
}

func getDbConnection(databaseCredentials cfgprovider.DatabaseCredentials, logger *lesqllog.Logger) (*sql.DB, error) {

	sEnc := base64.StdEncoding.EncodeToString([]byte(databaseCredentials.BigQueryCredentials))

	connectionString := fmt.Sprintf("bigquery://%s/eu/%s?credentials=%s", databaseCredentials.Host, databaseCredentials.Db, sEnc)

	db, err := sql.Open("bigquery", connectionString)
	if err != nil {
		logger.Fatal("", fmt.Sprintf("%s", err))
	}
	//defer db.Close()

	err = db.Ping()
	if err != nil {
		logger.Fatal("", fmt.Sprintf("%s", err))
	}

	return db, nil
}
