package postgresql

import (
	"crypto/tls"
	"crypto/x509"
	"database/sql"
	"fmt"
	"strconv"

	"github.com/jackc/pgx"
	"github.com/jackc/pgx/stdlib"
	"gitlab.com/lesql/lesql/src/lesqllog"
	"gitlab.com/lesql/lesql/src/libs/cfgprovider"
	"gitlab.com/lesql/lesql/src/libs/dbprovider"
)

// Driver is ...
type Driver struct{}

// GetDbConnection opens a new connection to the database. name is a connection string.
func (d *Driver) GetDbConnection(projectname string, settings cfgprovider.DatabaseCredentials, logger *lesqllog.Logger) (*sql.DB, error) {
	return getDbConnection(settings, logger)
}

func init() {
	dbprovider.Register("postgresql", &Driver{})
}

func getDbConnection(databaseCredentials cfgprovider.DatabaseCredentials, logger *lesqllog.Logger) (*sql.DB, error) {

	dbPort, err := strconv.Atoi(databaseCredentials.Port)
	if err != nil {
		logger.Fatal("Connect to database: error with port", err)
	}

	connConfig := pgx.ConnConfig{
		Host:     databaseCredentials.Host,
		Port:     uint16(dbPort),
		Database: databaseCredentials.Db,
		User:     databaseCredentials.User,
		Password: databaseCredentials.Password,
		// Logger:   logrusadapter.NewLogger(log),
		// LogLevel: 10,
	}

	//If certs are set,
	if databaseCredentials.Ca != "" && databaseCredentials.Cert != "" && databaseCredentials.CertKey != "" {
		rootCertPool := x509.NewCertPool()

		if ok := rootCertPool.AppendCertsFromPEM([]byte(databaseCredentials.Ca)); !ok {
			logger.Fatal("", "Failed to append PEM.")
		}

		certs, err := tls.X509KeyPair([]byte(databaseCredentials.Cert), []byte(databaseCredentials.CertKey))
		if err != nil {
			logger.Fatal(fmt.Sprintf("%s", err))
		}
		tlsConfig := tls.Config{
			RootCAs:      rootCertPool,
			ClientCAs:    rootCertPool,
			Certificates: []tls.Certificate{certs},
			ServerName:   databaseCredentials.Host,
		}
		connConfig.TLSConfig = &tlsConfig
	} else if databaseCredentials.Sslmode == "disable" || databaseCredentials.Sslmode == "false" {
		connConfig.TLSConfig = nil
	} else {
		connConfig.TLSConfig = &tls.Config{
			ServerName:         databaseCredentials.Host,
			InsecureSkipVerify: true,
		}
	}

	// log := logrus.New()
	// log.SetLevel(logrus.DebugLevel)

	// driverConfig := stdlib.DriverConfig{
	// 	ConnConfig: connConfig,
	// }

	//stdlib.RegisterDriverConfig(&driverConfig)

	db := stdlib.OpenDB(connConfig)

	err = db.Ping()
	if err != nil {
		logger.Fatal("", fmt.Sprintf("%s", err))
	}

	return db, nil
}
