package cfgprovider

import (
	"errors"
	"sync"

	"gitlab.com/lesql/lesql/src/lesqllog"
)

type Driver interface {
	GetDbCredentials(projectname string, settings StorageProviderSettings, logger *lesqllog.Logger) (DatabaseCredentials, error)
}

//StorageProviderSettings defines all needed data to get DatabaseCredentials from StorageProvider
type StorageProviderSettings struct {
	Endpoints []string
	User      string
	Password  string
	Location  string
	Key       string
	KeySalt   string
}

//DatabaseCredentials is the response of the storageprovider
type DatabaseCredentials struct {
	DbType              string `json:"type" yaml:"type"`
	Host                string
	Port                string
	User                string
	Password            string `json:"pass" yaml:"pass"`
	Db                  string
	Ca                  string
	Cert                string
	CertKey             string
	Sslmode             string
	BigQueryCredentials string `json:"bigQueryCredentials" yaml:"bigQueryCredentials"`
}

var (
	driversMu sync.RWMutex
	drivers   = make(map[string]Driver)
)

//Register registers a cfgprovider. Run it from init() in cfgprovider plugin
func Register(name string, driver Driver) {
	driversMu.Lock()

	defer driversMu.Unlock()

	if driver == nil {
		panic("cfgprovider: Register driver is nil")
	}

	if _, dup := drivers[name]; dup {
		panic("cfgprovider: Register called twice for driver " + name)
	}

	drivers[name] = driver

}

//GetDbCredentials gets the DatabaseCredentials from in config defined storageprovider
func GetDbCredentials(name string, projectName string, settings StorageProviderSettings, logger *lesqllog.Logger) (DatabaseCredentials, error) {

	logger.Debug("GetDbCredentials from storageprovider: " + name)

	if drivers[name] == nil {
		return DatabaseCredentials{}, errors.New("Unknown driver: " + name)
	}

	databaseCredentials, err := drivers[name].GetDbCredentials(projectName, settings, logger)
	if err != nil {
		return DatabaseCredentials{}, nil
	}

	return databaseCredentials, nil
}

//StorageProviderExists tests if a specific StorageProvider was registered
func StorageProviderExists(name string) bool {

	if _, ok := drivers[name]; ok {
		return true
	}

	return false
}
