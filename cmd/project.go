package cmd

import (
	"fmt"
	"strings"

	"gitlab.com/lesql/lesql/src/project"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(projectCmd)
}

var projectCmd = &cobra.Command{
	Use:   "project [project] workflow",
	Short: "Get DAG of project",
	Long:  `Get DAG of project`,
	Args:  cobra.ExactArgs(2),
	RunE: func(cmd *cobra.Command, args []string) error {

		switch strings.ToLower(args[1]) {
		case "workflow":
			return project.ShowWorkflow(args[0], vipercfg, logger)
		default:
			return fmt.Errorf("Unknown command %s", args[1])
		}
	},
}
