package cmd

import (
	"gitlab.com/lesql/lesql/src/lesqlinit"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(initCmd)
}

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initials models structure",
	Long:  `Initials models structure`,
	Run: func(cmd *cobra.Command, args []string) {
		lesqlinit.Run(vipercfg, logger)
	},
}
