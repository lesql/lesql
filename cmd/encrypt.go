// *
// *

package cmd

import (
	"bufio"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/lesql/lesql/src/run"
)

var (
//
)

func init() {

	rootCmd.AddCommand(encryptCmd)

}

var encryptCmd = &cobra.Command{
	Use:   "encrypt [string] | encrypt - < file",
	Short: "Encrypts a value for yaml",
	Long:  `Encrypts a value for yaml.`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {

		str := ""
		if args[0] == "-" {

			scanner := bufio.NewScanner(os.Stdin)
			for scanner.Scan() {
				str += scanner.Text()
			}

		} else {
			str = args[0]
		}

		run.Encrypt(vipercfg, logger, str)

	},
}
