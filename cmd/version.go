// *
// *

package cmd

import (
	"fmt"

	"github.com/common-nighthawk/go-figure"
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number",
	Long:  `Print the version number`,
	Run: func(cmd *cobra.Command, args []string) {
		myFigure := figure.NewFigure("lesql", "", true)
		myFigure.Print()
		fmt.Println()
		fmt.Println("Run your ETL with LESS SQL")
		fmt.Println()
		fmt.Println("lesql version " + version + "-" + commit + " " + goos + "/" + goarch)
		fmt.Println()
	},
}
