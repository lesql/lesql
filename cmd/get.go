package cmd

import (
	"fmt"
	"strings"

	"gitlab.com/lesql/lesql/src/get"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(getCmd)
}

var getCmd = &cobra.Command{
	Use:   "get [projects | models]",
	Short: "List projects & models",
	Long:  `List projects & models`,
	RunE: func(cmd *cobra.Command, args []string) error {
		switch strings.ToLower(args[0]) {
		case "projects":
			return get.Projects(vipercfg, logger)
		default:
			return fmt.Errorf("Unknown type %q, please specify `projects` or `models`", args[0])
		}
	},
}
