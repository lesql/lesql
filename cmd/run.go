// *
// *

package cmd

import (
	"fmt"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/lesql/lesql/src/run"
)

var (
	model           string
	dropTargetTable bool
	startDate       string
	endDate         string
	vars            string
)

func init() {
	cobra.OnInitialize(setRunCmdConfig)

	rootCmd.AddCommand(runCmd)

	runCmd.Flags().BoolVar(&dropTargetTable, "drop", false, `Drops target table before running transformations`)
	runCmd.Flags().StringVar(&endDate, "endDate", time.Now().Format("2006-01-02"), `This fills the 'endDate' template parameter with date`)
	runCmd.Flags().StringVarP(&model, "model", "m", "", `transfomration(s) to run:
	file => run only this transformation`)
	runCmd.Flags().StringVar(&startDate, "startDate", time.Now().Add(time.Hour*24*30*-1).Format("2006-01-02"), `This fills the 'startDate' template parameter with date`)
	runCmd.Flags().StringVar(&vars, "vars", "", `With this parameter, you can push parameters into models, global_models and macros.
Format has to be json.
Example: --vars '{"channel" : "sehrcool"}'`)
	runCmd.Flags().Bool("list-tags", false, "Show all tags within the workflow")
	runCmd.Flags().StringSlice("tags", []string{}, "Run only workflow jobs with the specified tags")
	runCmd.Flags().StringSlice("skip-tags", []string{}, "Exclude workflow jobs with the specified tags")

}

func setRunCmdConfig() {
	vipercfg.BindPFlag("list-tags", runCmd.Flags().Lookup("list-tags"))
	vipercfg.BindPFlag("tags", runCmd.Flags().Lookup("tags"))
	vipercfg.BindPFlag("skip-tags", runCmd.Flags().Lookup("skip-tags"))
}

var runCmd = &cobra.Command{
	Use:   "run [project name]",
	Short: "Run transformation(s)",
	Long:  `Run transformation(s)`,
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {

		projectName := args[0]

		vipercfg.SetDefault("dropTargetTable", dropTargetTable)
		vipercfg.SetDefault("endDate", endDate)
		vipercfg.SetDefault("model", model)
		vipercfg.SetDefault("startDate", startDate)
		vipercfg.SetDefault("vars", vars)

		if len(vipercfg.GetStringSlice("tags")) > 0 && len(vipercfg.GetStringSlice("skip-tags")) > 0 {
			return fmt.Errorf("--tags and --skip-tags cannot be combined")
		}

		run.Run(projectName, vipercfg, logger)
		return nil
	},
}
