// *
// *

package cmd

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/lesql/lesql/src/lesqllog"
)

const (
	bashCompletionFunc = `__lesql_get_projects()
{
	local lesql_output out
	if lesql_output=$(lesql get projects 2>/dev/null); then
	#if lesql_output=$(echo zzzdummy zzztest 2>/dev/null); then
		#out=($(echo "${lesql_output}" | awk '{print $1}'))
		#COMPREPLY=( $( compgen -W "${out[*]}" -- "$cur" ) )
		COMPREPLY=( $( compgen -W "${lesql_output[*]}" -- "$cur" ) )
	fi
}

__custom_func() {
case ${last_command} in
lesql_run | lesql_project)
			__lesql_get_projects
			return
			;;
	*)
			;;
esac
}
`
)

var (
	commit    string
	goarch    string
	goos      string
	cryptSalt string
	cfgFile   string
	//debug      bool
	quiet      bool
	basepath   string
	modelspath string
	version    string
	vipercfg   *viper.Viper
	logger     *lesqllog.Logger
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "lesql",
	Short: "lesql (less SQL) - SQL transfrom",
	Long: `lesql (less SQL) - SQL transfrom
With lesql bla blb `,
	BashCompletionFunction: bashCompletionFunc,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	//	Run: func(cmd *cobra.Command, args []string) { },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute(versionLocal string, osLocal string, archLocal string, commitLocal string, cryptSaltLocal string) {
	version = versionLocal
	goos = osLocal
	goarch = archLocal
	commit = commitLocal
	cryptSalt = cryptSaltLocal
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	// path, _ := os.Getwd()
	// log.Println("PATH", path)
	// log.Println(os.Getenv("GOPATH"))
	// log.Println(os.Getenv("PATH"))

	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().String("config", "", "specify a config file (default is ./config.toml)")
	rootCmd.PersistentFlags().BoolP("debug", "d", false, "Activate debug")
	rootCmd.PersistentFlags().String("macrospath", "globals/macros", "set macrospath")
	rootCmd.PersistentFlags().String("modelspath", "workflows", "set modelspath")
	rootCmd.PersistentFlags().String("modelspathsuffix", "lesql", "set modelspathsuffix")
	rootCmd.PersistentFlags().String("modelsglobalpath", "globals/models", "set global models path")
	rootCmd.PersistentFlags().String("logfile", "", "set path to logfile")
	rootCmd.PersistentFlags().String("salt", "", "salt for encryption and decryption")
	rootCmd.PersistentFlags().String("key", "", "key for encryption and decryption")
	rootCmd.PersistentFlags().Bool("uselogfile", false, "Write to logfile only if set to true")
	rootCmd.PersistentFlags().BoolP("quiet", "q", false, "Do not write anything stdout => you should use '--uselogfile'")

	//rootCmd.PersistentFlags().BoolVarP(&debug, "debug", "d", false, "Activate debug")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	//rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {

	vipercfg = viper.New()
	logger = lesqllog.New()

	vipercfg.SetEnvPrefix("lesql") // will be uppercased automatically
	vipercfg.AutomaticEnv()        // read in environment variables that match
	vipercfg.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	vipercfg.BindPFlag("logfile", rootCmd.Flags().Lookup("logfile"))
	vipercfg.BindPFlag("modelspath", rootCmd.Flags().Lookup("modelspath"))
	vipercfg.BindPFlag("modelspathsuffix", rootCmd.Flags().Lookup("modelspathsuffix"))
	vipercfg.BindPFlag("macrospath", rootCmd.Flags().Lookup("macrospath"))
	vipercfg.BindPFlag("modelsglobalpath", rootCmd.Flags().Lookup("modelsglobalpath"))
	vipercfg.BindPFlag("uselogfile", rootCmd.Flags().Lookup("uselogfile"))

	vipercfg.Set("cryptsalt", cryptSalt+vipercfg.GetString("salt"))
	vipercfg.Set("cryptkey", vipercfg.GetString("key"))

	vipercfg.BindPFlag("quiet", rootCmd.Flags().Lookup("quiet"))
	vipercfg.BindPFlag("debug", rootCmd.Flags().Lookup("debug"))
	vipercfg.BindPFlag("config", rootCmd.Flags().Lookup("config"))

	debug := vipercfg.GetBool("debug")
	quiet := vipercfg.GetBool("quiet")
	cfgFile := vipercfg.GetString("config")

	if quiet && debug {
		logger.Fatal("", "Cannot use 'quiet' and 'debug' at the same time")
	}

	if debug {
		err := logger.SetLevel("debug")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		logger.Debug("Debugging ON")
		vipercfg.Set("debug", true)
	} else if quiet {
		err := logger.SetLevel("quiet")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		vipercfg.Set("quiet", true)
	}

	if cfgFile != "" {
		// Use config file from the flag.
		vipercfg.SetConfigFile(cfgFile)
	} else {
		// 		// Find home directory.
		// 		home, err := homedir.Dir()
		// 		if err != nil {
		// 			fmt.Println(err)
		// 			os.Exit(1)
		// 		}
		// 		// Search config in home directory with name "config" (without extension).
		// 		vipercfg.AddConfigPath(home)

		// _, filename, _, ok := runtime.Caller(0)
		// if !ok {
		// 	logger.Fatal("","Can not get base path")
		// }
		// path := path.Join(path.Dir(filename), "..")

		//path, err := filepath.Abs(filepath.Dir(os.Args[0]))
		path, err := filepath.Abs(filepath.Dir("."))

		if err != nil {
			logger.Fatal("", "Can not get base path"+path)
		}
		logger.Debug("PATH: " + path)

		vipercfg.AddConfigPath(path)

		vipercfg.SetConfigName("config-lesql")

		logger.Debug("Loading config from: " + path)

		vipercfg.SetDefault("basepath", path)
	}

	// If a config file is found, read it in.
	if err := vipercfg.ReadInConfig(); err == nil {
		logger.Debug(fmt.Sprintf("Using config file: %s", vipercfg.ConfigFileUsed()))
	} else {
		logger.Debug(err)
	}

	if vipercfg.GetBool("uselogfile") {
		logger.Debug("Using logfile: " + vipercfg.GetString("logfile"))
		logger.SetLogFile(vipercfg.GetString("logfile"))
		logger.SetLogEntry(vipercfg.GetString("logentry"))
	}
}
