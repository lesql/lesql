package lesqlmain

import (
	"gitlab.com/lesql/lesql/cmd"
	_ "gitlab.com/lesql/lesql/src/libs/dbprovider/bigquery"
	_ "gitlab.com/lesql/lesql/src/libs/dbprovider/postgresql"
)

var (
	Version   = "development"
	Os        = "unknown"
	Arch      = "unknown"
	Commit    = "unknown"
	CryptSalt = "salt for client secret oeHzHWtF1LvSrKhPtqso37e66h4AvygeIC2hm3bqcNCo3p0eXywv8w3b1wTU"
)

func Run() {
	cmd.Execute(Version, Os, Arch, Commit, CryptSalt)
}
