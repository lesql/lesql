package main

import (
	"gitlab.com/lesql/lesql/lesqlmain"
)

var (
	version   = "development"
	os        = "unknown"
	arch      = "unknown"
	commit    = "unknown"
	cryptSalt = "salt for client secret gHj3HEI2se9oewnstaHRbfhaWgLP0kv8w3b1wTU8lvRGrf4U"
)

func main() {
	lesqlmain.Version = version
	lesqlmain.Os = os
	lesqlmain.Arch = arch
	lesqlmain.Commit = commit
	lesqlmain.CryptSalt = cryptSalt
	lesqlmain.Run()
}
