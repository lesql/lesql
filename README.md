# lesql - (less SQL) SQL transfrom #

[![pipeline status](https://gitlab.com/lesql/lesql/badges/main/pipeline.svg)](https://gitlab.com/lesql/lesql/-/commits/main)
[![coverage report](https://gitlab.com/lesql/lesql/badges/main/coverage.svg)](https://gitlab.com/lesql/lesql/-/commits/main)

* [Website](https://lesql.com)
* [Getting Started](https://lesql.com/docs/getting-started/)
* [Docs](https://lesql.com/docs/)

## Start developing ##

* Clone repos
* `go run main.go init` => This should download all dependencies and guide you through the setup of an example project/workflow
* Now run your first workflow: `LESQL_YAML_PASSWORD=$(cat ~/.lesql_yaml_password_dev) go run main.go -d run zzzexampleproject`

## Build ##

## Maintainace ##

Update go.mod: `go mod tidy`

## Tests ##

* Run Tests: `go test ./...`
* Run Tests: `go test ./... -cover`

### Update golden files ###

```shell
cd src/libs/templates
go test .
go test -update .
```
